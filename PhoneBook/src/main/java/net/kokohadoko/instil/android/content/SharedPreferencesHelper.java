package net.kokohadoko.instil.android.content;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import net.kokohadoko.instil.phonebook.R;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.util.Base64;
import android.util.Log;

/**
 * SharedPreferencesヘルパークラス
 * 
 * @author inuko
 * @since 0.0.1
 */
public class SharedPreferencesHelper {

	/** ログ出力用文字列 */
	private static final String LOG_TAG = SharedPreferencesHelper.class.getSimpleName();

	/** 鍵のbit数 */
	protected static final int ENCRYPT_KEY_LENGTH = 128;
	/** 暗号化キー */
	protected static final String KEY_SECRET_KEY = "secret_key";
	/** 暗号化アルゴリズム */
	protected static final String KEY_ALGORITHM = "AES";
	/** 暗号化キー */
	protected final Key key;
	/** コンテキスト */
	protected final Context context;
	/** SharedPreferences */
	protected static SharedPreferences pref;

	/** API用シークレットトークン */
	public static final String KEY_SECRET_TOKEN = "secret_token";
	
	/**
	 * デフォルトコンストラクタ
	 * 
	 * @since 0.0.1
	 * @param context コンテキスト
	 */
	public SharedPreferencesHelper(Context context) {
		this.context = context.getApplicationContext();
		
		// SharedPreferencesの取得
        Resources res = this.context.getResources();
        String preferencesId = res.getString(R.string.preferences_id);
		// Preferencesから暗号化キーを取得
		pref = this.context.getSharedPreferences(preferencesId, Context.MODE_PRIVATE);
		String keyStr = pref.getString(KEY_SECRET_KEY, null);
		
		if (keyStr == null) {
			// Preferencesから取得できなかった場合
			
			// 暗号化キーを生成
			key = generateKey();
			// 生成したキーを保存
			String base64Key = Base64.encodeToString(key.getEncoded(), Base64.URL_SAFE | Base64.NO_WRAP);
			// 
			Editor edit = pref.edit();
			edit.putString(KEY_SECRET_KEY, base64Key);
			edit.commit();
		} else {
			// Preferencesから取得できた場合
			// キーを復元
			byte[] keyBytes = Base64.decode(keyStr, Base64.URL_SAFE | Base64.NO_WRAP);
			key = new SecretKeySpec(keyBytes, KEY_ALGORITHM);
		}
	}

	/**
	 * 暗号化キーの生成をする。
	 * 
	 * @throws RuntimeException 暗号化キーの生成に失敗した場合 
	 * @return 暗号化キー
	 */
	public static Key generateKey() {
		try {
			KeyGenerator generator = KeyGenerator.getInstance(KEY_ALGORITHM);
			SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
			generator.init(ENCRYPT_KEY_LENGTH, random);
			return generator.generateKey();
		} catch (NoSuchAlgorithmException nsae) {
			Log.e("", nsae.getMessage());
			throw new RuntimeException(nsae);
		} catch (Exception e) {
			Log.e("", e.getMessage());
			throw new RuntimeException(e);
		}
	}

	/**
	 * 暗号化した文字列をSharedPreferencesに保存
	 * 
	 * @since 0.0.1
	 * @param key キー
	 * @param value 値
	 * @return 保存に成功した場合にtrue, 保存に失敗した場合にfalseを返す
	 */
	public boolean encrypt(String key, String value) {
		
		boolean flag = false;
		
		if (key != null && value != null) {
			try {
				Cipher cipher = Cipher.getInstance(KEY_ALGORITHM);
				cipher.init(Cipher.ENCRYPT_MODE, this.key);
				byte[] result = cipher.doFinal(value.getBytes());
				String encryptedStr = Base64.encodeToString(result, Base64.URL_SAFE | Base64.NO_WRAP);
				Editor edit = pref.edit();
				edit.putString(key, encryptedStr);
				edit.commit();
				flag = pref.contains(key);
			} catch (NoSuchAlgorithmException nsae) {
				Log.e(LOG_TAG, nsae.getMessage(), nsae);
			} catch (NoSuchPaddingException nspe) {
				Log.e(LOG_TAG, nspe.getMessage());
			} catch (InvalidKeyException ike) {
				Log.e(LOG_TAG, ike.getMessage(), ike);
			} catch (IllegalBlockSizeException ibse) {
				Log.e(LOG_TAG, ibse.getMessage(), ibse);
			} catch (BadPaddingException bpe) {
				Log.e(LOG_TAG, bpe.getMessage(), bpe);
			}
		}
		
		return flag;
	}

	/**
	 * 復号化した文字列を返却する
	 * 
	 * @since 0.0.1
	 * @param key キー
	 * @return 復号化した文字列
	 */
	public String decrypt(String key) {
		
		String decryptedStr = null;
		
		if (key != null) {
			String encryptedStr = pref.getString(key, null);
			if (encryptedStr != null) {
				try {
					Cipher cipher = Cipher.getInstance(KEY_ALGORITHM);
					cipher.init(Cipher.DECRYPT_MODE, this.key);
					byte[] result = cipher.doFinal(Base64.decode(encryptedStr, Base64.URL_SAFE | Base64.NO_WRAP));
					decryptedStr = new String(result);
				} catch (IllegalBlockSizeException ibse) {
					Log.e(LOG_TAG, ibse.getMessage(), ibse);
				} catch (BadPaddingException bpe) {
					Log.e(LOG_TAG, bpe.getMessage(), bpe);
				} catch (InvalidKeyException ike) {
					Log.e(LOG_TAG, ike.getMessage(), ike);
				} catch (NoSuchAlgorithmException nsae) {
					Log.e(LOG_TAG, nsae.getMessage(), nsae);
				} catch (NoSuchPaddingException nspe) {
					Log.e(LOG_TAG, nspe.getMessage(), nspe);
				}
			}
		}

		return decryptedStr;
	}
	
	/**
	 * 引数に一致するキーを持つ値を削除する
	 * 
	 * @since 0.0.1
	 * @param key キー
	 */
	public void remove(String key) {
		Editor edit = pref.edit();
		edit.remove(key);
		edit.commit();
	}

	/**
	 * 引数に一致するキーを持つ値を削除する
	 * 
	 * @since 0.0.1
	 * @param keys キー配列
	 */
	public void remove(String[] keys) {
		int len = keys.length;
		Editor edit = pref.edit();
		for (int i = 0; i < len; i++) {
			edit.remove(keys[i]);
		}
		edit.commit();
	}
	
	/**
	 * 引数に一致するキーを持っているか
	 * 
	 * @since 0.0.1
	 * @param key キー
	 * @return 一致するキーが存在する場合に true, そうでない場合に false を返す
	 */
	public boolean contains(String key) {
		return pref.contains(key);
	}

	/**
	 * 引数に一致する値を文字列で取得する
	 * （暗号化、複合化処理対象外）
	 * 
	 * @since 0.0.1
	 * @param key キー
	 * @param defValue 一致するキーが存在しない場合の初期値
	 * @return キーに一致する値の文字列
	 */
	public String getString(String key, String defValue) {
		return pref.getString(key, defValue);
	}

	/**
	 * 引数に一致する値をbooleanで取得する
	 * 
	 * @since 0.0.1
	 * @param key キー
	 * @param defValue 一致するキーが存在しない場合の初期値
	 * @return キーに一致する値の文字列
	 */
	public boolean getBoolean(String key, boolean defValue) {
		return pref.getBoolean(key, defValue);
	}

	/**
	 * 引数に一致する値をintで取得する
	 * 
	 * @since 0.0.1
	 * @param key キー
	 * @param defValue 一致するキーが存在しない場合の初期値
	 * @return キーに一致する値の文字列
	 */
	public int getInt(String key, int defValue) {
		return pref.getInt(key, defValue);
	}

	/**
	 * エディターを取得する
	 * 
	 * @since 0.0.1
	 * @return エディター
	 */
	public Editor edit() {
		return pref.edit();
	}

	/**
	 * 編集をコミットする
	 * 
	 * @since 0.0.1
	 * @param editor エディター
	 * @return コミットに成功した場合にtrue, そうでない場合に falseを返す
	 */
	public boolean commit(Editor editor) {
		return editor.commit();
	}
}