package net.kokohadoko.instil.phonebook;

/**
 * 連絡先の項目に対して検索対象とするかどうかを構成するクラス
 * 
 * @author inuko
 * @since 0.0.1
 */
public class SearchableFields extends Fields {

	/** 検索対象とする場合のキー文字列 */
	private static final String KEY = "searchable_"; 

	/** 表示名を表す */
	public static final String DISPLAY_NAME = KEY + Fields.DISPLAY_NAME;
	/** ふりがなを表す */
	public static final String PHONETIC_NAME = KEY + Fields.PHONETIC_NAME;
	/** 着信音を表す */
	public static final String CUSTOM_RINGTONE = KEY + Fields.CUSTOM_RINGTONE;
	/** メモを表す */
	public static final String NOTE = KEY + Fields.NOTE;
	/** 会社を表す */
	public static final String COMPANY = KEY + Fields.COMPANY;
	/** 役職を表す */
	public static final String TITLE = KEY + Fields.TITLE;
	/** メールアドレスを表す */
	public static final String EMAIL = KEY + Fields.EMAIL;
	/** イベントを表す */
	public static final String EVENT = KEY + Fields.EVENT;
	/** ニックネームを表す */
	public static final String NICKNAME = KEY + Fields.NICKNAME;
	/** 関係を表す */
	public static final String RELATION = KEY + Fields.RELATION;
	/** 電話番号を表す */
	public static final String NUMBER = KEY + Fields.NUMBER;

	/**
	 * デフォルトコンストラクタ
	 * 
	 * @since 0.0.1
	 */
	public SearchableFields() {
	}
}