package net.kokohadoko.instil.phonebook.database.sqlite;

import net.kokohadoko.instil.phonebook.ConfirmContacts;
import net.kokohadoko.instil.phonebook.CustomRingtone;
import net.kokohadoko.instil.phonebook.IncomingCall;
import net.kokohadoko.instil.phonebook.ReplyMessage;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * SQLiteヘルパークラス
 * 
 * @author inuko
 * @since 0.0.1
 */
public class SQLiteHelper extends SQLiteOpenHelper {

	/** ログ出力用文字列 */
	private static final String LOG_TAG = SQLiteHelper.class.getSimpleName();

	/**
	 * コンストラクタ
	 * 
	 * @since 1.0.0
	 * @param context コンテキスト
	 * @param name データベースファイル名
	 * @param factory 
	 * @param version バージョン
	 */
	public SQLiteHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.beginTransaction();
		try {
			db.execSQL(CustomRingtone.getSQLForCreateTable());
			db.execSQL(ConfirmContacts.getSQLForCreateTable());
			db.execSQL(IncomingCall.getSQLForCreateTable());
			db.execSQL(ReplyMessage.getCreateTableSQL());

			db.setTransactionSuccessful();
		} catch (IllegalStateException ise) {
			Log.e(LOG_TAG, ise.getLocalizedMessage());
		} finally {
			db.endTransaction();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if (oldVersion < newVersion) {
			db.execSQL("DROP TABLE IF EXISTS " + CustomRingtone.TABLE_NAME);
			db.execSQL("DROP TABLE IF EXISTS " + ConfirmContacts.TABLE_NAME);
			db.execSQL("DROP TABLE IF EXISTS " + IncomingCall.TABLE_NAME);
			db.execSQL("DROP TABLE IF EXISTS " + ReplyMessage.TABLE_NAME);
			// 
			onCreate(db);
		}
	}
}