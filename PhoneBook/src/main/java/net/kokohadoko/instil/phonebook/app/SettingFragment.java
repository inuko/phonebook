package net.kokohadoko.instil.phonebook.app;

import net.kokohadoko.instil.android.content.SharedPreferencesHelper;
import net.kokohadoko.instil.phonebook.R;
import net.kokohadoko.instil.phonebook.SearchableFields;
import net.kokohadoko.instil.phonebook.VisibleFields;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

/**
 * 設定画面を構成するFragment
 * 
 * @author inuko
 * @since 0.0.1
 */
public class SettingFragment extends Fragment implements OnClickListener, CompoundButton.OnCheckedChangeListener {

	/** ログ出力用文字列 */
	private static final String LOG_TAG = SettingFragment.class.getSimpleName();

	/** SharedPreferencesヘルパー */
	private SharedPreferencesHelper pref;

	/** 表示名（表示制御） */
	private CheckBox checkBoxVisibleDisplayName;
	/** ふりがな（表示制御） */
	private CheckBox checkBoxVisiblePhoneticName;

	/** 表示名（検索制御） */
	private CheckBox checkBoxSearchableDisplayName;
	/** ふりがな（検索制御） */
	private CheckBox checkBoxSearchablePhoneticName;
	
	/**
	 * デフォルトコンストラクタ
	 * 
	 * @since 0.0.1
	 */
	public SettingFragment() {
	}

	/**
	 * 初期化されたSettingFragmentインスタンスを生成して返す
	 * 
	 * @since 0.0.1
	 * @return 初期化されたSetteingFragmentインスタンス
	 */
	public static SettingFragment newInstance() {
		SettingFragment fragment = new SettingFragment();
		return fragment;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// 
		pref = new SharedPreferencesHelper(getActivity().getApplicationContext());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_setting, null, false);

		TextView textViewConfirm = (TextView) view.findViewById(R.id.textView_confirm);
		textViewConfirm.setOnClickListener(this);
		
		//Switch switchConfirm = (Switch) view.findViewById(R.id.switch_confirm);
		//switchConfirm.setOnCheckedChangeListener(this);
		
		TextView textViewReply = (TextView) view.findViewById(R.id.textView_reply);
		textViewReply.setOnClickListener(this);
		
		//Switch switchReply = (Switch) view.findViewById(R.id.switch_reply);
		//switchReply.setOnCheckedChangeListener(this);

		// 表示項目
		// 表示名
		checkBoxVisibleDisplayName = (CheckBox) view.findViewById(R.id.checkBox_visibleDisplayName);
		checkBoxVisibleDisplayName.setOnCheckedChangeListener(this);
		// ふりがな
		checkBoxVisiblePhoneticName = (CheckBox) view.findViewById(R.id.checkBox_visiblePhoneticName);
		checkBoxVisiblePhoneticName.setOnCheckedChangeListener(this);
		// 検索項目
		// 表示名
		checkBoxSearchableDisplayName = (CheckBox) view.findViewById(R.id.checkBox_searchableDisplayName);
		checkBoxSearchableDisplayName.setOnCheckedChangeListener(this);
		// ふりがな
		checkBoxSearchablePhoneticName = (CheckBox) view.findViewById(R.id.checkBox_searchablePhoneticName);
		checkBoxSearchablePhoneticName.setOnCheckedChangeListener(this);

		return view;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onResume() {
		super.onResume();
		// 表示項目
		// 表示名
		checkBoxVisibleDisplayName.setChecked(pref.getBoolean(VisibleFields.DISPLAY_NAME, false));
		// ふりがな
		checkBoxVisiblePhoneticName.setChecked(pref.getBoolean(VisibleFields.PHONETIC_NAME, false));
		// 検索項目
		// 表示名
		checkBoxSearchableDisplayName.setChecked(pref.getBoolean(SearchableFields.DISPLAY_NAME, false));
		// ふりがな
		checkBoxSearchablePhoneticName.setChecked(pref.getBoolean(SearchableFields.PHONETIC_NAME, false));
	}

	/**
	 * {@inheritDoc}
	 */
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		switch (buttonView.getId()) {
			case R.id.checkBox_visibleDisplayName:
			{
				Editor editor = pref.edit();
				editor.putBoolean(VisibleFields.DISPLAY_NAME, isChecked);
				editor.commit();
			}
				break;

			case R.id.checkBox_visiblePhoneticName:
			{
				Editor editor = pref.edit();
				editor.putBoolean(VisibleFields.PHONETIC_NAME, isChecked);
				editor.commit();
			}
				break;

			case R.id.checkBox_searchableDisplayName:
			{
				Editor editor = pref.edit();
				editor.putBoolean(SearchableFields.DISPLAY_NAME, isChecked);
				editor.commit();
			}
				break;

			case R.id.checkBox_searchablePhoneticName:
			{
				Editor editor = pref.edit();
				editor.putBoolean(SearchableFields.PHONETIC_NAME, isChecked);
				editor.commit();
			}
				break;

			default:
				break;
		}
		/*
		switch (buttonView.getId()) {
			case R.id.switch_confirm:
			{
				// 
				pref = this.getActivity().getSharedPreferences("pref", Activity.MODE_PRIVATE);
				Editor e = pref.edit();
				e.putBoolean("confirm", isChecked);
				// データの保存
				e.commit();
			}
				break;
				
			case R.id.switch_reply:
			{
				// 
				pref = this.getActivity().getSharedPreferences("pref", Activity.MODE_PRIVATE);
				Editor e = pref.edit();
				e.putBoolean("reply", isChecked);
				// データの保存
				e.commit();
			}
				break;
				
			default:
				break;
		}
		*/
	}

	/**
	 * {@inheritDoc}
	 */
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.textView_confirm:
			{
				FragmentManager fm = this.getFragmentManager();
				FragmentTransaction ft = fm.beginTransaction();
				ConfirmListFragment fragment = ConfirmListFragment.newInstance();
				ft.addToBackStack(null);
				ft.replace(R.id.content_frame, fragment);
				ft.commit();
			}
				break;
				
			case R.id.textView_reply:
			{
				FragmentManager fm = this.getFragmentManager();
				FragmentTransaction ft = fm.beginTransaction();
				ReplyListFragment fragment = ReplyListFragment.newInstance();
				ft.addToBackStack(null);
				ft.replace(R.id.content_frame, fragment);
				ft.commit();
			}
				break;

			default:
				break;
		}
	}
}