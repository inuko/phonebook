package net.kokohadoko.instil.phonebook;

/**
 * 連絡先の項目に対して表示対象とするかどうかを構成するクラス
 * 
 * @author inuko
 * @since 0.0.1
 */
public class VisibleFields extends Fields {

	/** 表示対象とする場合のキー文字列 */
	private static final String KEY = "visible_"; 

	/** 表示名を表す */
	public static final String DISPLAY_NAME = KEY + Fields.DISPLAY_NAME;
	/** ふりがなを表す */
	public static final String PHONETIC_NAME = KEY + Fields.PHONETIC_NAME;
	/** 着信音を表す */
	public static final String CUSTOM_RINGTONE = KEY + Fields.CUSTOM_RINGTONE;

	/**
	 * デフォルトコンストラクタ
	 * 
	 * @since 0.0.1
	 */
	public VisibleFields() {
	}
}