package net.kokohadoko.instil.phonebook;

/**
 * 連絡先の項目の電話番号を構成するクラス
 * 
 * @author inuko
 * @since 0.0.1
 */
public class Phone {

	/** ログ出力用文字列 */
	private static final String LOG_TAG = Phone.class.getSimpleName();

	/** 番号 */
	private String number;
	/** 種類 */
	private int type;
	/** ラベル */
	private String label;

	/**
	 * コンストラクタ
	 * 
	 * @since 0.0.1
	 * @param number 番号
	 * @param type 種類
	 * @param label ラベル
	 */
	public Phone(String number, int type, String label) {
		this.number = number;
		this.type = type;
		this.label = label;
	}

	/**
	 * 番号を取得する
	 * 
	 * @since 0.0.1
	 * @return 番号
	 * @see {@link #setNumber(String)}
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * 番号を設定する
	 * 
	 * @since 0.0.1
	 * @param number 番号
	 * @see {@link #getNumber()}
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * 種類を取得する
	 * 
	 * @since 0.0.1
	 * @return 種類
	 * @see {@link #setType(int)}
	 */
	public int getType() {
		return type;
	}

	/**
	 * 種類を設定する
	 * 
	 * @since 0.0.1
	 * @param type 種類
	 * @see {@link #getType()}
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * ラベルを取得する
	 * 
	 * @since 0.0.1
	 * @return ラベル
	 * @see {@link #setLabel(String)}
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * ラベルを設定する
	 * 
	 * @since 0.0.1
	 * @param label ラベル
	 * @see {@link #getLabel()}
	 */
	public void setLabel(String label) {
		this.label = label;
	}
}