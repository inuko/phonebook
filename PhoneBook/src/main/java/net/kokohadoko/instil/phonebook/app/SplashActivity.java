package net.kokohadoko.instil.phonebook.app;

import net.kokohadoko.instil.phonebook.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

/**
 * スプラッシュ画面を構成するアクティビティ
 *
 * @author inuko
 * @since  0.0.1
 */
public class SplashActivity extends Activity {

    /** ログ出力用文字列 */
    private static final String LOG_TAG = SplashActivity.class.getSimpleName();

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);
        // スプラッシュ画面からの遷移を行う
        Handler handler = new Handler();
        handler.postDelayed(new SplashHandler(), 1000);

    }

    /**
     * スプラッシュ画面からの遷移を行う
     *
     * @author inuko
     * @since 0.0.1
     */
    private class SplashHandler implements Runnable {

    	/**
    	 * {@inheritDoc}
    	 */
        public void run() {
            Intent intent = new Intent(getBaseContext(), MainActivity.class);
            startActivity(intent);
            finish();
        }
    }
}