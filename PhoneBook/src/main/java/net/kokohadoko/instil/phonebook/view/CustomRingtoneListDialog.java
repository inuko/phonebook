package net.kokohadoko.instil.phonebook.view;

import java.util.ArrayList;

import net.kokohadoko.instil.phonebook.R;
import android.app.AlertDialog;
import android.app.Dialog;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * 着信音選択ダイアログを構成するDialogFragment
 * 
 * @author inuko
 * @since 0.0.1
 */
public class CustomRingtoneListDialog extends DialogFragment implements OnItemClickListener {

	/** コールバックインターフェース */
	public static interface Callbacks {

		/**
		 * 着信音が選択された場合に呼ばれる
		 * 
		 * @since 0.0.1
		 * @param title 着信音タイトル
		 * @param uri URI
		 * @param lookupKey ルックアップキー
		 */
		public void onClickCustomRingtone(String title, Uri uri, String lookupKey);
	}

	/** ログ出力用 */
	public static final String LOG_TAG = CustomRingtoneListDialog.class.getSimpleName();

	/** IDを表す文字列 */
	private static final String IDS = "ids";
	/** タイトルを表す文字列 */
	private static final String TITLES = "titles";
	/** URIを表す文字列 */
	private static final String URIS = "uris";
	/** ルックアップキーを表す文字列 */
	private static final String LOOKUP_KEY = "lookup";

	/** ListView */
	private ListView mListView;

	/**
	 * デフォルトコンストラクタ
	 * 
	 * @since 0.0.1
	 */
	public CustomRingtoneListDialog() {
	}

	/**
	 * 初期化されたCustomRingtoneListDialogインスタンスを生成する
	 * 
	 * @since 0.0.1
	 * @param cursor カーソル
	 * @return 初期化されたCustomRingtoneListDialogインスタンス
	 */
	public static CustomRingtoneListDialog newInstance(Cursor cursor) {
		return newInstance(cursor, null);
	}

	/**
	 * 初期化されたCustomRingtoneListDialogインスタンスを生成する
	 * 
	 * @since 0.0.1
	 * @param cursor カーソル
	 * @param lookupKey ルックアップキー
	 * @return 初期化されたCustomRingtoneListDialogインスタンス
	 */
	public static CustomRingtoneListDialog newInstance(Cursor cursor, String lookupKey) {
		CustomRingtoneListDialog fragment = new CustomRingtoneListDialog();
		
		ArrayList<String> ids = new ArrayList<String>();
		ArrayList<String> titleList = new ArrayList<String>();
		ArrayList<String> uriList = new ArrayList<String>();
		while (cursor.moveToNext()) {
			long id = cursor.getLong(RingtoneManager.ID_COLUMN_INDEX);
			ids.add(String.valueOf(id));
			
			String title = cursor.getString(RingtoneManager.TITLE_COLUMN_INDEX);
			titleList.add(title);
			
			String uriString = cursor.getString(RingtoneManager.URI_COLUMN_INDEX);
			Uri uri = Uri.withAppendedPath(Uri.parse(uriString), String.valueOf(id));
			
			uriList.add(uri.toString());
		}
		Bundle args = new Bundle(cursor.getCount());
		args.putStringArrayList(IDS, ids);
		args.putStringArrayList(TITLES, titleList);
		args.putStringArrayList(URIS, uriList);
		if (lookupKey != null) {
			args.putString(LOOKUP_KEY, lookupKey);
		}

		fragment.setArguments(args);
		return fragment;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Dialog dialog = super.onCreateDialog(savedInstanceState); 
		return dialog;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		Bundle args = getArguments();
		ArrayList<String> titles = args.getStringArrayList(TITLES);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity().getApplicationContext());
		
		
		View view = inflater.inflate(R.layout.listdialog_custom_ringtone, null, false);
		
		mListView = (ListView) view.findViewById(R.id.listView);
		mListView.setOnItemClickListener(this);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1);
		int size = titles.size();
		for (int i = 0; i < size; i++) {
			String title = titles.get(i);
			adapter.add(title);
		}
		mListView.setAdapter(adapter);
		
		return view;
	}

	/**
	 * {@inheritDoc}
	 */
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		
		final Callbacks callbacks;

		if (getTargetFragment() != null && getTargetFragment() instanceof Callbacks) {
			callbacks = (Callbacks) getTargetFragment();
		} else if (getActivity() instanceof Callbacks) {
			callbacks = (Callbacks) getActivity();
		} else {
			callbacks = null;
		}

		if (callbacks != null) {
			Bundle args = getArguments();
			ArrayList<String> titles = args.getStringArrayList(TITLES);
			ArrayList<String> uris = args.getStringArrayList(URIS);
			String lookupKey = args.getString(LOOKUP_KEY);

			String title = titles.get(position);
			Uri uri = Uri.parse(uris.get(position));
			
			callbacks.onClickCustomRingtone(title, uri, lookupKey);
		}

		getDialog().dismiss();
	}
}