package net.kokohadoko.instil.phonebook.widget;

import java.util.HashMap;
import java.util.Map;

import net.kokohadoko.instil.android.content.SharedPreferencesHelper;
import net.kokohadoko.instil.phonebook.Fields;
import net.kokohadoko.instil.phonebook.R;
import net.kokohadoko.instil.phonebook.VisibleFields;
import android.content.Context;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.QuickContactBadge;
import android.widget.TextView;

/**
 * 連絡先の一覧を保持するアダプター
 * 
 * @author inuko
 * @since 0.0.1
 */
public class ContactsCursorAdapter extends CursorAdapter {

	/** ログ出力用文字列 */
	private static final String LOG_TAG = ContactsCursorAdapter.class.getSimpleName();

	/** LayoutInflater */
	private LayoutInflater inflater;
	/** SharedPreferencesヘルパー */
	private SharedPreferencesHelper pref;
	/** 端末内の着信音の一覧をキャッシュする */
	private static Map<String, String> customRingtoneMap = new HashMap<String, String>();

	/**
	 * コンストラクタ
	 *
	 * @since 0.0.0
	 * @param context コンテキスト
	 * @param c カーソル
	 */
	public ContactsCursorAdapter(Context context, Cursor c) {
		super(context, c, false);
	
		inflater = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		pref = new SharedPreferencesHelper(context);

		customRingtoneMap.clear();
		Ringtone defaltRingtone = RingtoneManager.getRingtone(context, RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE));
		String defalutRingtoneTitleString = defaltRingtone.getTitle(context);

		RingtoneManager ringtoneManager = new RingtoneManager(context);
		Cursor cursor = ringtoneManager.getCursor();
		while (cursor.moveToNext()) {
			long id = cursor.getLong(RingtoneManager.ID_COLUMN_INDEX);
			String title = cursor.getString(RingtoneManager.TITLE_COLUMN_INDEX);
			String uriString = cursor.getString(RingtoneManager.URI_COLUMN_INDEX);

			Uri uri = Uri.withAppendedPath(Uri.parse(uriString), String.valueOf(id));
			customRingtoneMap.put(uri.toString(), title);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void bindView(View view, final Context context, Cursor c) {

		final long contactId = c.getInt(c.getColumnIndexOrThrow(Fields.RAW_CONTACTS_ID));
		final String lookupKey = c.getString(c.getColumnIndexOrThrow(Fields.LOOKUP));
		final String displayName = c.getString(c.getColumnIndexOrThrow(Fields.DISPLAY_NAME));
		final String photoThumbnailUri = c.getString(c.getColumnIndexOrThrow(Fields.PHOTO_THUMBNAIL_URI));
		final String customRingtone = c.getString(c.getColumnIndexOrThrow(Fields.CUSTOM_RINGTONE));

		final QuickContactBadge quickContact = (QuickContactBadge) view.findViewById(R.id.quickContactBadge);
		if (photoThumbnailUri != null) {
			quickContact.setImageURI(Uri.parse(photoThumbnailUri));
			quickContact.setMode(ContactsContract.QuickContact.MODE_MEDIUM);
			quickContact.assignContactUri(Contacts.getLookupUri(contactId, lookupKey));
		} else {
			quickContact.setImageToDefault();
			quickContact.setMode(ContactsContract.QuickContact.MODE_MEDIUM);
			quickContact.assignContactUri(Contacts.getLookupUri(contactId, lookupKey));
		}

		TextView textViewDisplayName = (TextView) view.findViewById(R.id.textView_displayName);
		textViewDisplayName.setText(displayName);

		TextView textViewRingtoneTitle = (TextView) view.findViewById(R.id.textView_ringtoneTitle);
		textViewRingtoneTitle.setText(customRingtone);

		// 表示名
		if (pref.getBoolean(VisibleFields.DISPLAY_NAME, false)) {
			
		} else {
			
		}
		// ふりがな
		if (pref.getBoolean(VisibleFields.PHONETIC_NAME, false)) {
			
		} else {
		
		}
		// 着信音
		if (pref.getBoolean(VisibleFields.CUSTOM_RINGTONE, false)) {
			
		} else {
			
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public View newView(Context context, Cursor c, ViewGroup parent) {
		View view = inflater.inflate(R.layout.row_contact, null, false);
		return view;
	}
}
