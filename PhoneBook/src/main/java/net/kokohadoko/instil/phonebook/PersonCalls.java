package net.kokohadoko.instil.phonebook;

import java.util.Iterator;
import java.util.List;

/**
 * 連絡先毎の通話履歴を構成するクラス
 * 
 * @author inuko
 * @since 0.0.1
 */
public class PersonCalls extends Person {

	/** ログ出力用文字列 */
	private static final String LOG_TAG = PersonCalls.class.getSimpleName();
	/** 通話履歴 */
	private Calls calls;

	/**
	 * コンストラクタ
	 * 
	 * @since 0.0.1
	 * @param rawContactsId 連絡先ID
	 * @param lookup ルックアップキー
	 * @param photoThumbnailUri サムネイル画像URI
	 * @param hasPhoneNumber 電話番号をもっているかどうか
	 * @param lastTimeContacted 最終連絡日時
	 * @param customRingtone 着信音
	 */
	public PersonCalls(long rawContactsId, String lookup,
			String photoThumbnailUri, int hasPhoneNumber,
			String lastTimeContacted, String customRingtone) {
		super(rawContactsId, lookup, photoThumbnailUri, hasPhoneNumber, lastTimeContacted, customRingtone);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] toObjects() {
		return super.toObjects();
	}

	/**
	 * オブジェクトが検索条件に一致するかどうかを返す
	 * 
	 * @since 0.0.1
	 * @param query 抽出条件
	 * @return オブジェクトが検索条件に一致する場合に true, そうでない場合に false を返す
	 */
	public static boolean isMatchesNumber(PersonCalls personCalls, String number) {
		if (personCalls != null && number != null) {
			List<Phone> phones = personCalls.getPhone();
			Iterator<Phone> itr = phones.iterator();
			while (itr.hasNext()) {
				Phone phone = itr.next();
				String phoneNumber = phone.getNumber();
				if (phoneNumber != null && phoneNumber.equals(number)) {
					return true;
				}
			}
		}

		return false;
	}
}