package net.kokohadoko.instil.phonebook;

import android.database.Cursor;
import android.provider.CallLog;

/**
 * 電話履歴を構成するクラス
 * 
 * @author inuko
 * @since 0.0.1
 */
public class Calls {

	/** ログ出力用文字列 */
	private static final String LOG_TAG = Calls.class.getSimpleName();

	/** 電話番号 */
	public static final String NUMBER = CallLog.Calls.NUMBER;
	/** 日付 */
	public static final String DATE = CallLog.Calls.DATE;
	/** 時間 */
	public static final String DURATION = CallLog.Calls.DURATION;
	/** 種類 */
	public static final String TYPE = CallLog.Calls.TYPE;

	/** 電話番号 */
	private String number;
	/** 日付 */
	private String date;
	/** 時間 */
	private long duration;
	/** 種類 */
	private int type;

	/**
	 * コンストラクタ
	 * 
	 * @since 0.0.1
	 * @param cursor カーソル
	 */
	public Calls(Cursor cursor) {
		number = cursor.getString(cursor.getColumnIndexOrThrow(CallLog.Calls.NUMBER));
		date = cursor.getString(cursor.getColumnIndexOrThrow(CallLog.Calls.DATE));
		duration = cursor.getLong(cursor.getColumnIndexOrThrow(CallLog.Calls.DURATION));
		type = cursor.getInt(cursor.getColumnIndexOrThrow(CallLog.Calls.TYPE));
	}

	/**
	 * 電話番号を取得する
	 * 
	 * @since 0.0.1
	 * @return 電話番号
	 * @see {@link #setNumber(String)}
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * 電話番号を設定する
	 * 
	 * @since 0.0.1
	 * @param number 電話番号
	 * @see {@link #getNumber()}
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * 日付を取得する
	 * 
	 * @since 0.0.1
	 * @return 日付
	 * @see {@link #setDate(String)}
	 */
	public String getDate() {
		return date;
	}

	/**
	 * 日付を設定する
	 * 
	 * @since 0.0.1
	 * @param date 日付
	 * @see {@link #getDate()}
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * 時間を取得する
	 * 
	 * @since 0.0.1
	 * @return 時間
	 * @see {@link #setDuration(long)}
	 */
	public long getDuration() {
		return duration;
	}

	/**
	 * 時間を設定する
	 * 
	 * @since 0.0.1
	 * @param duration 時間
	 * @see {@link #getDuration()}
	 */
	public void setDuration(long duration) {
		this.duration = duration;
	}

	/**
	 * 種類を取得する
	 * 
	 * @since 0.0.1
	 * @return 種類
	 * @see {@link #setType(int)}
	 */
	public int getType() {
		return type;
	}

	/**
	 * 種類を設定する
	 * 
	 * @since 0.0.1
	 * @param type 種類
	 * @see {@link #getType()}
	 */
	public void setType(int type) {
		this.type = type;
	}
}