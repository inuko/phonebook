package net.kokohadoko.instil.phonebook.content;

import net.kokohadoko.instil.phonebook.ConfirmContacts;
import net.kokohadoko.instil.phonebook.CustomRingtone;
import net.kokohadoko.instil.phonebook.database.sqlite.SQLiteHelper;
import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.text.TextUtils;

/**
 * 
 * 
 * @author inuko
 * @since 1.0.0
 */
public class PhoneBookContentProvider extends ContentProvider {

	/**  */
	public static final String AUTHORITY = "net.kokohadoko.instil.phonebook.provider";
	/**  */
	private static final String SQLITE_FILENAME = "net.kokohadoko.instil.phonebook.sqlite";
	/**  */
	private SQLiteHelper mOpenHelper;
	/**  */
	private static final int CUSTOM_RINGTONE = 0;
	/**  */
	private static final int CUSTOM_RINGTONE_ID = 1;
	/**  */
	private static final int CONFIRM = 2;
	/**  */
	private static final int CONFIRM_ID = 3;

	/**  */
	private static final UriMatcher uriMatcher;
	static {
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI(AUTHORITY, CustomRingtone.TABLE_NAME, CUSTOM_RINGTONE);
		uriMatcher.addURI(AUTHORITY, CustomRingtone.TABLE_NAME + "/#", CUSTOM_RINGTONE_ID);
		uriMatcher.addURI(AUTHORITY, ConfirmContacts.TABLE_NAME, CONFIRM);
		uriMatcher.addURI(AUTHORITY, ConfirmContacts.TABLE_NAME + "/#", CONFIRM_ID);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getType(Uri uri) {
		switch (uriMatcher.match(uri)) {
			case CUSTOM_RINGTONE:
				return CustomRingtone.CONTENT_TYPE;

			case CUSTOM_RINGTONE_ID:
				return CustomRingtone.CONTENT_ITEM_TYPE;
				
			case CONFIRM:
				return ConfirmContacts.CONTENT_TYPE;
				
			case CONFIRM_ID:
				return ConfirmContacts.CONTENT_ITEM_TYPE;

			default:
				throw new IllegalArgumentException("Unknown URI:" + uri);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Uri insert(Uri uri, ContentValues values) {
		Uri returnUri;
		
		switch (uriMatcher.match(uri)) {
			case CUSTOM_RINGTONE:
			{
				SQLiteDatabase db = mOpenHelper.getWritableDatabase();
				final long rowId =  db.insertOrThrow(CustomRingtone.TABLE_NAME, null, values);
				returnUri = ContentUris.withAppendedId(uri, rowId);
				getContext().getContentResolver().notifyChange(returnUri, null);
			}
				break;
				
			case CONFIRM:
			{
				SQLiteDatabase db = mOpenHelper.getWritableDatabase();
				final long rowId =  db.insertOrThrow(ConfirmContacts.TABLE_NAME, null, values);
				returnUri = ContentUris.withAppendedId(uri, rowId);
				getContext().getContentResolver().notifyChange(returnUri, null);
			}
				break;	
				
			default:
				throw new IllegalArgumentException("Unknown URI" + uri);
		}
		
		return returnUri;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean onCreate() {
		mOpenHelper = new SQLiteHelper(getContext(), SQLITE_FILENAME, null, 3);
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		
		Cursor cursor = null;
		
		switch (uriMatcher.match(uri)) {
			case CUSTOM_RINGTONE:
			{
				SQLiteDatabase db = mOpenHelper.getReadableDatabase();
				cursor = db.query(CustomRingtone.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
				cursor.setNotificationUri(getContext().getContentResolver(), uri);
			}
				break;
				
			case CUSTOM_RINGTONE_ID:
			{
				String id = uri.getPathSegments().get(1);
				StringBuffer buf = new StringBuffer();
				buf.append(CustomRingtone._ID).append(" = ").append(id);
				if (!TextUtils.isEmpty(selection)) {
					buf.append(" AND (").append(selection).append(")");
				}

				SQLiteDatabase db = mOpenHelper.getReadableDatabase();
				cursor = db.query(CustomRingtone.TABLE_NAME, projection, buf.toString(), selectionArgs, null, null, sortOrder);
				cursor.setNotificationUri(getContext().getContentResolver(), uri);
			}
				break;
				
			case CONFIRM:
			{
				SQLiteDatabase db = mOpenHelper.getReadableDatabase();
				cursor = db.query(ConfirmContacts.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
				cursor.setNotificationUri(getContext().getContentResolver(), uri);
			}
				break;
				
			case CONFIRM_ID:
			{
				String id = uri.getPathSegments().get(1);
				StringBuffer buf = new StringBuffer();
				buf.append(ConfirmContacts._ID).append(" = ").append(id);
				if (!TextUtils.isEmpty(selection)) {
					buf.append(" AND (").append(selection).append(")");
				}

				SQLiteDatabase db = mOpenHelper.getReadableDatabase();
				cursor = db.query(ConfirmContacts.TABLE_NAME, projection, buf.toString(), selectionArgs, null, null, sortOrder);
				cursor.setNotificationUri(getContext().getContentResolver(), uri);
			}
				break;
		
			default:
				throw new IllegalArgumentException("Unknown URI:" + uri);
		}
		
		return cursor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		
		int count;
		
		switch (uriMatcher.match(uri)) {
			case CUSTOM_RINGTONE:
			{
				// 
				SQLiteDatabase db = mOpenHelper.getWritableDatabase();
				count = db.update(CustomRingtone.TABLE_NAME, values, selection, selectionArgs);
				getContext().getContentResolver().notifyChange(uri, null);
			}
				break;
		
			case CUSTOM_RINGTONE_ID:
			{
				// 
				String id = uri.getPathSegments().get(1);
				StringBuffer buf = new StringBuffer();
				buf.append(CustomRingtone._ID).append(" = ").append(id);
				if (!TextUtils.isEmpty(selection)) {
					buf.append(" AND (").append(selection).append(")");
				}
				// 
				SQLiteDatabase db = mOpenHelper.getWritableDatabase();
				count = db.update(CustomRingtone.TABLE_NAME, values, buf.toString(), selectionArgs);
				getContext().getContentResolver().notifyChange(uri, null);
			}
				break;
				
			case CONFIRM:
			{
				// 
				SQLiteDatabase db = mOpenHelper.getWritableDatabase();
				count = db.update(ConfirmContacts.TABLE_NAME, values, selection, selectionArgs);
				getContext().getContentResolver().notifyChange(uri, null);
			}
				break;
		
			case CONFIRM_ID:
			{
				// 
				String id = uri.getPathSegments().get(1);
				StringBuffer buf = new StringBuffer();
				buf.append(ConfirmContacts._ID).append(" = ").append(id);
				if (!TextUtils.isEmpty(selection)) {
					buf.append(" AND (").append(selection).append(")");
				}
				// 
				SQLiteDatabase db = mOpenHelper.getWritableDatabase();
				count = db.update(ConfirmContacts.TABLE_NAME, values, buf.toString(), selectionArgs);
				getContext().getContentResolver().notifyChange(uri, null);
			}
				break;
								
			default:
				throw new IllegalArgumentException("Unknown URI:" + uri);
		}
		
		return count;
	}
}