package net.kokohadoko.instil.phonebook.widget;

import java.util.List;

import net.kokohadoko.instil.phonebook.ContactAccount;
import net.kokohadoko.instil.phonebook.R;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 
 * 
 * @author inuko
 * @since 0.0.1
 */
public class AccountSpinnerAdapter extends ArrayAdapter<ContactAccount> {

	/** LayoutInflater */
	private LayoutInflater mLayoutInflater;
	/** PackageManager */
	private PackageManager mPackageManager;

	/**
	 * コンストラクタ
	 * 
	 * @since 0.0.1
	 * @param context コンテキスト
	 * @param resource リソースID
	 * @param objects 連絡先アカウント群
	 */
	public AccountSpinnerAdapter(Context context, int resource,
			List<ContactAccount> objects) {
		super(context, resource, objects);
	
		mLayoutInflater = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mPackageManager = context.getPackageManager();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		View view;
		if (convertView == null) {
			view = mLayoutInflater.inflate(R.layout.account_spinner_dropdown_item, parent, false); 
		} else {
			view = convertView;
		}
		
		ContactAccount item = getItem(position);
		
		CharSequence name = mPackageManager.getText(item.getPackageName(), item.getLabelId(), null);
		Drawable smallIconDrawable = mPackageManager.getDrawable(item.getPackageName(), item.getSmallIconId(), null);
		
		ImageView imageViewIcon = (ImageView) view.findViewById(R.id.icon);
		imageViewIcon.setImageDrawable(smallIconDrawable);
		TextView textViewTitle = (TextView) view.findViewById(R.id.title);
		textViewTitle.setText(name.toString());
		TextView textViewSubTitle = (TextView) view.findViewById(R.id.subTitle);
		textViewSubTitle.setText(item.getName());
		
		return view;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view;
		if (convertView == null) {
			view = mLayoutInflater.inflate(R.layout.account_spinner_item, parent, false); 
		} else {
			view = convertView;
		}
		
		ContactAccount item = getItem(position);
		
		CharSequence name = mPackageManager.getText(item.getPackageName(), item.getLabelId(), null);
		Drawable smallIconDrawable = mPackageManager.getDrawable(item.getPackageName(), item.getSmallIconId(), null);
		
		ImageView imageViewIcon = (ImageView) view.findViewById(R.id.icon);
		imageViewIcon.setImageDrawable(smallIconDrawable);
		TextView textViewTitle = (TextView) view.findViewById(R.id.title);
		textViewTitle.setText(name.toString());
		TextView textViewSubTitle = (TextView) view.findViewById(R.id.subTitle);
		textViewSubTitle.setText(item.getName());

		return view;
	}
}
