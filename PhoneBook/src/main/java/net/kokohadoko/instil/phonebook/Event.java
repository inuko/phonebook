package net.kokohadoko.instil.phonebook;

/**
 * 連絡先の項目のイベントを構成するクラス
 * 
 * @author inuko
 * @since 0.0.1
 */
public class Event {

	/** ログ出力用文字列 */
	private static final String LOG_TAG = Event.class.getSimpleName();

	/** ラベル */
	private String label;
	/** 種類 */
	private int type;

	/**
	 * デフォルトコンストラクタ
	 * 
	 * @since 0.0.1
	 * @param label ラベル
	 * @param type 種類
	 */
	public Event(String label, int type) {
		this.label = label;
		this.type = type;
	}

	/**
	 * ラベルを取得する
	 * 
	 * @since 0.0.1
	 * @return ラベル
	 * @see {@link #setLabel(String)}
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * ラベルを設定する
	 * 
	 * @since 0.0.1
	 * @param label ラベル
	 * @see {@link #getLabel()}
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * 種類を取得する
	 * 
	 * @since 0.0.1
	 * @return 種類
	 * @see {@link #setType(int)}
	 */
	public int getType() {
		return type;
	}

	/**
	 * 種類を設定する
	 * 
	 * @since 0.0.1
	 * @param type 種類
	 * @see {@link #getType()}
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * オブジェクトが検索条件に一致するかどうかを返す
	 * 
	 * @since 0.0.1
	 * @param query 抽出条件
	 * @return オブジェクトが検索条件に一致する場合に true, そうでない場合に false を返す
	 */
	public boolean isMatch(String query) {
		if (query != null) {
			if (label != null && label.contains(query)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}