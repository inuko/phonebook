package net.kokohadoko.instil.phonebook;

/**
 * 電話履歴と連絡先を統合して構成するクラス
 * 
 * @author inuko
 * @since 0.0.1
 */
public class CallLogs {

	/** ログ出力用文字列 */
	private static final String LOG_TAG = CallLogs.class.getSimpleName();

	/** 連絡先ID */
	private long rawContactsId;
	/** ルックアップキー */
	private String lookup;
	/** サムネイル画像URI */
	private String photoThumbnailUri;
	/** 表示名 */
	private String displayName;
	/** 電話番号 */
	private String number;
	/** 日付 */
	private String date;
	/** 時間 */
	private long duration;
	/** 種別 */
	private int type;

	/**
	 * コンストラクタ
	 * 
	 * @since 0.0.1
	 * @param rawContactId 連絡先ID
	 * @param lookup ルックアップキー
	 * @param photoThumbnailUri サムネイル画像
	 * @param displayName 表示名
	 * @param number 電話番号
	 * @param date 日付
	 * @param duration 時間
	 * @param type 種類
	 */
	public CallLogs(
			long rawContactId, String lookup, String photoThumbnailUri, String displayName,
			String number, String date, long duration, int type) {
		this.rawContactsId = rawContactId;
		this.lookup = lookup;
		this.photoThumbnailUri = photoThumbnailUri;
		this.displayName = displayName;
		this.number = number;
		this.date = date;
		this.duration = duration;
		this.type = type;
	}

	/**
	 * カラム名の一覧を返す
	 * 
	 * @since 0.0.1
	 * @return カラム名の一覧
	 */
	public static String[] getColumnNames() {
		return new String[] {
				Fields.RAW_CONTACTS_ID,
				Fields.LOOKUP,
				Fields.PHOTO_THUMBNAIL_URI,
				Fields.DISPLAY_NAME,
				Calls.NUMBER,
				Calls.DATE,
				Calls.DURATION,
				Calls.TYPE
		};
	}

	/**
	 * 連絡先IDを取得する
	 * 
	 * @since 0.0.1
	 * @return 連絡先ID
	 * @see {@link #setRawContactsId(long)}
	 */
	public long getRawContactsId() {
		return rawContactsId;
	}

	/**
	 * 連絡先IDを設定する
	 * 
	 * @since 0.0.1
	 * @param rawContactsId 連絡先ID
	 * @see {@link #getRawContactsId()}
	 */
	public void setRawContactsId(long rawContactsId) {
		this.rawContactsId = rawContactsId;
	}

	/**
	 * ルックアップキーを取得する
	 * 
	 * @since 0.0.1
	 * @return ルックアップキー
	 * @see {@link #setLookup(String)}
	 */
	public String getLookup() {
		return lookup;
	}

	/**
	 * ルックアップキーを設定する
	 * 
	 * @since 0.0.1
	 * @param lookup ルックアップキー
	 * @see {@link #getLookup()}
	 */
	public void setLookup(String lookup) {
		this.lookup = lookup;
	}

	/**
	 * サムネイル画像URIを取得する
	 * 
	 * @since 0.0.1
	 * @return サムネイル画像URI
	 * @see {@link #setPhotoThumbnailUri(String)}
	 */
	public String getPhotoThumbnailUri() {
		return photoThumbnailUri;
	}

	/**
	 * サムネイル画像URIを設定する
	 * 
	 * @since 0.0.1
	 * @param photoThumbnailUri サムネイル画像URI
	 * @see {@link #getPhotoThumbnailUri()}
	 */
	public void setPhotoThumbnailUri(String photoThumbnailUri) {
		this.photoThumbnailUri = photoThumbnailUri;
	}

	/**
	 * 表示名を取得する
	 * 
	 * @since 0.0.1
	 * @return 表示名
	 * @see {@link #setDisplayName(String)}
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * 表示名を設定する
	 * 
	 * @since 0.0.1
	 * @param displayName 表示名
	 * @see {@link #getDisplayName()}
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * 電話番号を取得する
	 * 
	 * @since 0.0.1
	 * @return number 電話番号
	 * @see {@link #setNumber(String)}
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * 電話番号を設定する
	 * 
	 * @since 0.0.1
	 * @param number 電話番号
	 * @see {@link #getNumber()}
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * 日付を取得する
	 * 
	 * @since 0.0.1
	 * @return 日付
	 * @see {@link #setDate(String)}
	 */
	public String getDate() {
		return date;
	}

	/**
	 * 日付を設定する
	 * 
	 * @since 0.0.1
	 * @param date 日付
	 * @see {@link #getDate()}
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * 時間を取得する
	 * 
	 * @since 0.0.1
	 * @return 時間
	 * @see {@link #setDuration(long)}
	 */
	public long getDuration() {
		return duration;
	}

	/**
	 * 時間を設定する
	 * 
	 * @since 0.0.1
	 * @param duration 時間
	 * @see {@link #getDuration()}
	 */
	public void setDuration(long duration) {
		this.duration = duration;
	}

	/**
	 * 種類を取得する
	 * 
	 * @since 0.0.1
	 * @return 種類
	 * @see {@link #setType(int)}
	 */
	public int getType() {
		return type;
	}

	/**
	 * 種類を設定する
	 * 
	 * @since 0.0.1
	 * @param type 種類
	 * @see {@link #getType()}
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * オブジェクト配列に変換して返す
	 * 
	 * @since 0.0.1
	 * @return オブジェクト配列に変換されたもの
	 */
	public Object[] toObjects() {
		return new Object[] {
				rawContactsId,
				lookup,
				photoThumbnailUri,
				displayName,
				number,
				date,
				duration,
				type
		};
	}
}