package net.kokohadoko.instil.phonebook.app;

import java.util.ArrayList;

import net.kokohadoko.instil.phonebook.ContactAccount;
import net.kokohadoko.instil.phonebook.R;
import net.kokohadoko.instil.phonebook.widget.AccountSpinnerAdapter;
import android.R.integer;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AuthenticatorDescription;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.OnNavigationListener;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

/**
* アプリのメインアクティビティ
* 
* @author inuko
* @since 0.0.1
*/
public class MainActivity extends ActionBarActivity implements OnNavigationListener {

	/** ログ出力用文字列 */
	private static final String LOG_TAG = MainActivity.class.getSimpleName();
	/** ActionBar Drawer Toggle */
	private ActionBarDrawerToggle mDrawerToggle;
	/** Navigation Drawer Layout */
	private DrawerLayout mDrawerLayout;
	/** アカウント一覧を保持するアダプター */
	private AccountSpinnerAdapter mAdapter;

	/** メインアクティビティ用のMENUグループID */
	public static final int MENU_GROUP_MAIN = 1;
	/** 連絡先一覧画面用のMENUグループID */
	public static final int MENU_GROUP_CONTRACT = 2;
	/** 電話履歴一覧画面用のMENUグループID */
	public static final int MENU_GROUP_HISTORIES = 3;
	/** ドロワー画面用のMENUグループID */
	public static final int MENU_GROUP_DRAWER = 4;

	/** メインアクティビティ用のMENU ITEM ID */
	public static final int MENU_ITEM_SETTINGS = 1;

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// レイアウトの設定
		setContentView(R.layout.activity_main);

		// ActionBarの設定
		final ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);
		// DrawerLayoutの生成
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
		// DrawerToggleの生成
	    mDrawerToggle = new ActionBarDrawerToggle(
	       		this, 
	       		mDrawerLayout, 
	       		R.drawable.ic_drawer,
	       		R.string.drawer_open, 
	       		R.string.drawer_close) {

	    	/* (非 Javadoc)
			 * @see android.support.v4.app.ActionBarDrawerToggle#onDrawerClosed(android.view.View)
			 */
			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
			}

			/* (非 Javadoc)
			 * @see android.support.v4.app.ActionBarDrawerToggle#onDrawerOpened(android.view.View)
			 */
			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
			}    	
	    };
	    mDrawerLayout.setDrawerListener(mDrawerToggle);

	    // アカウント一覧を取得し、アプリで利用する書式に変換する
	    final AccountManager accountManager = AccountManager.get(this);
	    Account[] accounts = accountManager.getAccounts();
	    AuthenticatorDescription[] descriptions = accountManager.getAuthenticatorTypes();

	    int accountLength = accounts.length;
	    int descriptionLength = descriptions.length;
	    ArrayList<ContactAccount> objects = new ArrayList<ContactAccount>();
	    ContactAccount contactsAccount = null;
	    for (int i = 0; i < accountLength; i++) {
			for (int j = 0; j < descriptionLength; j++) {				
				if (accounts[i].type.equals(descriptions[j].type)) {
					
					contactsAccount = new ContactAccount(
												accounts[i].name,
												descriptions[j].packageName,
												accounts[i].type,
												descriptions[j].labelId,
												descriptions[j].iconId,
												descriptions[j].smallIconId);
					objects.add(contactsAccount);
				}
			}
		}

	    actionBar.setDisplayShowTitleEnabled(false);
	    actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

	    mAdapter = new AccountSpinnerAdapter(this, R.layout.account_spinner_dropdown_item, objects);
	    actionBar.setListNavigationCallbacks(mAdapter, this);
	    
	    if (objects.isEmpty()) {
	    	// TODO:アカウントがない場合に表示するFragmentを用意する
	    	
	    }
	}

	/**
	 * {@inheritDoc}
	 */
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
	    // DrawerToggle       
		mDrawerToggle.syncState();
	}

    /**
	 * {@inheritDoc}
	 */
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		//	DrawerToggle
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		} else {
			
			switch (item.getGroupId()) {
				case MENU_GROUP_MAIN:
				{
					switch (item.getItemId()) {
						case MENU_ITEM_SETTINGS:
						{
							Intent intent = new Intent(getApplicationContext(), SettingActivity.class);
							startActivity(intent);
						}
							break;
	
						default:
							break;
					}
				}
					break;

				default:
					break;
			}
		}

		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public boolean onNavigationItemSelected(int position, long id) {

		ContactAccount item = mAdapter.getItem(position);	
		
		FragmentManager fm = this.getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		DrawerFragment fragment = DrawerFragment.newInstance(item.getName(), item.getType());
		ft.replace(R.id.left_drawer, fragment);
		ft.commit();
		
		return false;
	}

	
}