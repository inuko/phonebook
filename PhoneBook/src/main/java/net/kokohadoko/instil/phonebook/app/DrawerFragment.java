package net.kokohadoko.instil.phonebook.app;

import net.kokohadoko.instil.phonebook.R;
import net.kokohadoko.instil.phonebook.widget.GroupsCursorAdapter;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Groups;
import android.provider.ContactsContract.RawContacts;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.TextView;

/**
 * ドロワーメニューを構成するListFragment
 * 
 * @author inuko
 * @since 0.0.1
 */
public class DrawerFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor> {
	
	/** ログ出力用文字列 */
	private static final String LOG_TAG = DrawerFragment.class.getSimpleName();
	/**  */
	private static final String POSITION = "position";
	/**  */
	private static final int POSITION_NONE = 0;

	/** グループ一覧を保持するアダプター */
	private GroupsCursorAdapter mAdapter;
	/**  */
	private int position;
	/**  */
	private TextView textViewGroupsCount;
	/**  */
	private TextView textViewContactsCount;

	/**
	 * デフォルトコンストラクタ
	 * 
	 * @since 0.0.1
	 */
	public DrawerFragment() {
	}

	/**
	 * 初期化されたDrawerFragmentインスタンスを生成する
	 * 
	 * @since 0.0.1
	 * @param accountName アカウント名
	 * @param accountType アカウント種別
	 * @return 初期化されたDrawerFragmentインスタンス
	 */
	public static DrawerFragment newInstance(String accountName, String accountType) {
		DrawerFragment fragment = new DrawerFragment();
		Bundle args = new Bundle(3);
		args.putString(Groups.ACCOUNT_NAME, accountName);
		args.putString(Groups.ACCOUNT_TYPE, accountType);
		args.putInt(POSITION, POSITION_NONE);
		fragment.setArguments(args);
		return fragment;
	}

	/**
	 * 初期化されたDrawerFragmentインスタンスを生成する
	 * 
	 * @since 0.0.1
	 * @param accountName アカウント名
	 * @param accountType アカウント種別
	 * @param position 選択状態とする位置
	 * @return 初期化されたDrawerFragmentインスタンス
	 */
	public static DrawerFragment newInstance(String accountName, String accountType, int position) {
		DrawerFragment fragment = new DrawerFragment();
		Bundle args = new Bundle(3);
		args.putString(Groups.ACCOUNT_NAME, accountName);
		args.putString(Groups.ACCOUNT_TYPE, accountType);
		args.putInt(POSITION, position);
		fragment.setArguments(args);
		return fragment;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = (View) inflater.inflate(R.layout.fragment_drawer, null, false);

		// 
		textViewGroupsCount = (TextView) view.findViewById(R.id.textView_groups_count);
		textViewGroupsCount.setText("0");
		// 
		textViewContactsCount = (TextView) view.findViewById(R.id.textView_contacts_count);
		textViewContactsCount.setText("0");

		return view;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		mAdapter = new GroupsCursorAdapter(
					this.getActivity().getApplicationContext(),
					null);
		getListView().setAdapter(mAdapter);
		
		setListAdapter(mAdapter);
		
		registerForContextMenu(getListView());
		
		final Bundle args = getArguments();
		getLoaderManager().restartLoader(0, args, this);
		getLoaderManager().restartLoader(1, args, this);
	}

	/* (非 Javadoc)
	 * @see android.support.v4.app.Fragment#onContextItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		
		switch (item.getGroupId()) {
			case MainActivity.MENU_GROUP_DRAWER:
			{
				AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
				
				Cursor cursor = (Cursor) mAdapter.getItem(info.position);
			}
				break;
	
			default:
				break;
		}

		return super.onContextItemSelected(item);
	}

	/* (非 Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateContextMenu(android.view.ContextMenu, android.view.View, android.view.ContextMenu.ContextMenuInfo)
	 */
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);

		AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
		Cursor cursor = (Cursor) mAdapter.getItem(info.position);

		final String title = cursor.getString(cursor.getColumnIndexOrThrow(Groups.TITLE));

		menu.setHeaderTitle(title);
		menu.add(MainActivity.MENU_GROUP_DRAWER, 0, 0, "グループ名を編集");
		menu.add(MainActivity.MENU_GROUP_DRAWER, 1, 0, "グループを削除");
		menu.add(MainActivity.MENU_GROUP_DRAWER, 2, 0, "連絡先を追加する");
		menu.add(MainActivity.MENU_GROUP_DRAWER, 3, 0, "履歴を削除");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
//		super.onListItemClick(l, v, position, id);
		Cursor c = (Cursor) l.getItemAtPosition(position);

		long groupId = c.getLong(c.getColumnIndexOrThrow(Groups._ID));
		String title = c.getString(c.getColumnIndexOrThrow(Groups.TITLE));
		String accountName = c.getString(c.getColumnIndexOrThrow(Groups.ACCOUNT_NAME));
		String accountType = c.getString(c.getColumnIndexOrThrow(Groups.ACCOUNT_TYPE));

		FragmentManager fm = getFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		GroupsPagerFragment fragment = GroupsPagerFragment.newInstance(groupId, accountName, accountType, title);
		ft.replace(R.id.content_frame, fragment);
		ft.commit();
		
	}

	/**
	 * {@inheritDoc}
	 */
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {

		CursorLoader loader = null;
		
		switch (id) {
			case 0:
			{
				final String accountName = args.getString(Groups.ACCOUNT_NAME);
				final String accountType = args.getString(Groups.ACCOUNT_TYPE);

				loader = new CursorLoader(
						getActivity().getApplicationContext(),
						Groups.CONTENT_URI,
						null,
						Groups.ACCOUNT_NAME + " = ? AND " + Groups.ACCOUNT_TYPE + " = ? AND " + Groups.DELETED + " = 0",
						new String[]{ accountName, accountType },
						null
				);		
			}
				break;
				
			case 1:
			{
				final String accountName = args.getString(Groups.ACCOUNT_NAME);
				final String accountType = args.getString(Groups.ACCOUNT_TYPE);

				loader = new CursorLoader(
						getActivity().getApplicationContext(),
						ContactsContract.RawContacts.CONTENT_URI,
						new String[] { RawContacts._ID },
						RawContacts.ACCOUNT_NAME + " = ? AND " + RawContacts.ACCOUNT_TYPE + " = ? AND " + RawContacts.DELETED + " = 0",
						new String[] { accountName, accountType },
						null);
			}
				break;
				
			default:
				break;
		}

		return loader;
/*
		CursorLoader loader = new CursorLoader(
									getActivity().getApplicationContext(),
									Groups.CONTENT_URI,
									new String[]{ Groups._ID, Groups.SOURCE_ID, Groups.TITLE, Groups.ACCOUNT_NAME, Groups.ACCOUNT_TYPE, Groups.GROUP_VISIBLE },
									Groups.ACCOUNT_NAME + " = ? AND " + Groups.ACCOUNT_TYPE + " = ?",
									new String[]{ accountName, accountType },
									null);
*/
		
	}

	/**
	 * {@inheritDoc}
	 */
	public void onLoadFinished(Loader<Cursor> loader, Cursor c) {
		
		switch (loader.getId()) {
			case 0:
			{
				MatrixCursor mc = new MatrixCursor(new String[]{
						Groups._ID,
						Groups.TITLE,
						Groups.ACCOUNT_NAME,
						Groups.ACCOUNT_TYPE,
						Groups.GROUP_VISIBLE
					});

				final Bundle args = getArguments();
				// グループ設定なし
						mc.addRow(new Object[] {
							-1,
							"My Contacts",
							args.getString(Groups.ACCOUNT_NAME),
							args.getString(Groups.ACCOUNT_TYPE),
							1
						});
				
				if (c.moveToFirst()) {
					do {
						long id = c.getLong(c.getColumnIndexOrThrow(Groups._ID));
						String title = c.getString(c.getColumnIndexOrThrow(Groups.TITLE));
						String accountName = c.getString(c.getColumnIndexOrThrow(Groups.ACCOUNT_NAME));
						String accountType = c.getString(c.getColumnIndexOrThrow(Groups.ACCOUNT_TYPE));
						int groupVisible = c.getInt(c.getColumnIndexOrThrow(Groups.GROUP_VISIBLE));
						
						// すべての連絡先は弾く
						if (!title.equals("My Contacts")) {
							mc.addRow(new Object[] {
									id,
									title,
									accountName,
									accountType,
									groupVisible
							});
						}
					} while (c.moveToNext());
				}
				
				mAdapter.swapCursor(mc);
				
				if (mAdapter.isEmpty()) {
					// TODO:
					
				} else {
					final int position = args.getInt(POSITION, POSITION_NONE);
					final int count = mAdapter.getCount();
					if (position < count) {
				
						Cursor cursor = (Cursor) mAdapter.getItem(position);
						
						final long groupId = cursor.getLong(cursor.getColumnIndexOrThrow(Groups._ID));
						final String title = cursor.getString(cursor.getColumnIndexOrThrow(Groups.TITLE));
						final String accountName = cursor.getString(cursor.getColumnIndexOrThrow(Groups.ACCOUNT_NAME));
						final String accountType = cursor.getString(cursor.getColumnIndexOrThrow(Groups.ACCOUNT_TYPE));
						
						Handler handler = new Handler();
						handler.post(new Runnable() {
				
							public void run() {
								FragmentManager fm = getFragmentManager();
								FragmentTransaction ft = fm.beginTransaction();
								GroupsPagerFragment fragment = GroupsPagerFragment.newInstance(groupId, accountName, accountType, title);
								ft.replace(R.id.content_frame, fragment);
								ft.commit();
							}
						});
					} else {
						// TODO:
						
					}
				}
				
				//  グループ数の反映
				//textViewGroupsCount.setText(String.valueOf(mAdapter.getCount()));
			}
				break;
				
			case 1:
			{
				int count = c.getCount();
				//textViewContactsCount.setText(String.valueOf(count));
			}
				break;
				
			default:
				break;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void onLoaderReset(Loader<Cursor> loader) {
		mAdapter.swapCursor(null);
	}
}