package net.kokohadoko.instil.phonebook.content;

import net.kokohadoko.instil.android.content.SharedPreferencesHelper;
import net.kokohadoko.instil.phonebook.app.AlertDialogActivity;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * 電話発信時に割り込むためにイベントを取得するBroadcastReceiverを構成するクラス
 * 
 * @author inuko
 * @since 0.0.1
 */
public class CallsReceiver extends BroadcastReceiver {

	/** ログ出力用文字列 */
	private static final String LOG_TAG = CallsReceiver.class.getSimpleName();
	/** SharedPreferences内の発信確認設定を表す文字列 */
	public static final String PREF_KEY_CONFIRM = "confirm"; 

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onReceive(Context context, Intent intent) {

		SharedPreferencesHelper pref = new SharedPreferencesHelper(context);

		boolean isConfirm = pref.getBoolean(PREF_KEY_CONFIRM, false);
		if (isConfirm) {
			String extraPhoneNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
			String originalUri = intent.getStringExtra("android.phone.extra.ORIGINAL_URI");
			if (originalUri != null) {
				if (originalUri.indexOf("#Confirm") != -1) {
					setResultData(extraPhoneNumber);
				} else {
					setResultData(null);
				}
			} else {
				
				// TODO:除外リストにあるかどうか
				
				// TODO:未登録に割り込む場合は全部の連絡先とチェックする
				
				// TODO:各種プリファレンスの値を見てマッチング処理
				// TODO:電話長登録外への発信に割り込み
				// TODO:すべての発信に割り込み
				// TODO:割り込まない
				
				
				
				
				
				Intent dialog = new Intent(context, AlertDialogActivity.class);
				dialog.putExtra(Intent.EXTRA_PHONE_NUMBER, extraPhoneNumber);
				PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, dialog, 0);
				try {
					pendingIntent.send();
				} catch (CanceledException ce) {
					Log.e(LOG_TAG, ce.getMessage());
				}

				setResultData(null);
			}
		}
	}
}