package net.kokohadoko.instil.phonebook;

import net.kokohadoko.instil.phonebook.content.PhoneBookContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.ContactsContract.Groups;

/**
 * 不在着信があった場合の自動返信メッセージを構成するクラス
 * 
 * @author inuko
 * @since 0.0.1
 */
public class ReplyMessage implements BaseColumns {

	/** テーブル固有のCONTENT_URI．ContentResolverからこれを使用してアクセスを行う． */
	public static final Uri CONTENT_URI = Uri.parse("content://" + PhoneBookContentProvider.AUTHORITY + "/confirm");
	/** MIME_TYPE（複数） */
	public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.instil.reply_message";
	/** MIME_TYPE（単数） */
	public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.instil.reply_message";
	/** テーブル名 */
	public static final String TABLE_NAME = "reply_message";

	/** ルックアップキー（カラム名）を表す */
	public static final String LOOKUP_KEY = "lookup";
	/** メッセージ（カラム名）を表す */
	public static final String MESSAGE = "message";

	/** アカウント名 */
	private String accountName;
	/** アカウント種別 */
	private String accountType;
	/** ルックアップキー */
	private String lookupKey;
	/** メッセージ */
	private String message;

	/**
	 * デフォルトコンストラクタ
	 * 
	 * @since 0.0.1
	 */
	public ReplyMessage() {
	}

	/**
	 * コンストラクタ
	 * 
	 * @since 0.0.1
	 * @param cursor カーソル
	 */
	public ReplyMessage(Cursor cursor) {
		accountName = cursor.getString(cursor.getColumnIndexOrThrow(Groups.ACCOUNT_NAME));
		accountType = cursor.getString(cursor.getColumnIndexOrThrow(Groups.ACCOUNT_TYPE));
		lookupKey = cursor.getString(cursor.getColumnIndexOrThrow(LOOKUP_KEY));
		message = cursor.getString(cursor.getColumnIndexOrThrow(MESSAGE));
	}

	/**
	 * アカウント名を取得する
	 * 
	 * @since 0.0.1
	 * @return アカウント名
	 * @see {@link #setAccountName(String)}
	 */
	public String getAccountName() {
		return accountName;
	}

	/**
	 * アカウント名を設定する
	 * 
	 * @since 0.0.1
	 * @param accountName アカウント名
	 * @see {@link #getAccountName()}
	 */
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	/**
	 * アカウント種別を取得する
	 * 
	 * @since 0.0.1
	 * @return アカウント種別
	 * @see {@link #setAccountType(String)}
	 */
	public String getAccountType() {
		return accountType;
	}

	/**
	 * アカウント種別を設定する
	 * 
	 * @since 0.0.1
	 * @param accountType アカウント種別
	 * @see {@link #getAccountType()}
	 */
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	/**
	 * ルックアップキーを取得する
	 * 
	 * @since 0.0.1
	 * @return ルックアップキー
	 * @see {@link #setLookupKey(String)}
	 */
	public String getLookupKey() {
		return lookupKey;
	}

	/**
	 * ルックアップキーを設定する
	 * 
	 * @since 0.0.1
	 * @param lookupKey ルックアップキー
	 * @see {@link #getLookupKey()}
	 */
	public void setLookupKey(String lookupKey) {
		this.lookupKey = lookupKey;
	}

	/**
	 * 返信メッセージを取得する
	 * 
	 * @since 0.0.1
	 * @return 返信メッセージ
	 * @see {@link #setMessage(String)}
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * 返信メッセージを設定する
	 * 
	 * @since 0.0.1
	 * @param message 返信メッセージ
	 * @see {@link #getMessage()}
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * ContentValues形式に変換する
	 * 
	 * @since 0.0.1
	 * @return ContentValues形式に変換されたオブジェクト
	 */
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues(4);
		values.put(Groups.ACCOUNT_NAME, accountName);
		values.put(Groups.ACCOUNT_TYPE, accountType);
		values.put(LOOKUP_KEY, lookupKey);
		values.put(MESSAGE, message);
		return values;
	}

	/**
	 * SQLite用のCREATE TABLE文を生成する
	 * 
	 * @since 0.0.1
	 * @return CREATE TABLE文
	 */
	public static String getCreateTableSQL() {
		StringBuffer buf = new StringBuffer();
		buf.append("CREATE TABLE ").append(TABLE_NAME).append("(");
		buf.append(_ID).append(" INTEGER PRIMARY_KEY, ");
		buf.append(Groups.ACCOUNT_NAME).append(" TEXT NOT NULL, ");
		buf.append(Groups.ACCOUNT_TYPE).append(" TEXT NOT NULL, ");
		buf.append(LOOKUP_KEY).append(" TEXT NOT NULL, ");
		buf.append(MESSAGE).append(" TEXT NOT NULL");		
		buf.append(")");
		return buf.toString();
	}
}
