package net.kokohadoko.instil.phonebook.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

/**
 * BroadCastReceiverから呼び出される通知用アクティビティ
 * 
 * @author inuko
 * @since 0.0.1
 */
public class AlertDialogActivity extends FragmentActivity {

	/** ログ出力用文字列 */
	private static final String LOG_TAG = AlertDialogActivity.class.getSimpleName();

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Intent intent = getIntent();
		if (intent.hasExtra(Intent.EXTRA_PHONE_NUMBER)) {
			String extraPhoneNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);

			CallsAlertDialogFragment fragment = CallsAlertDialogFragment.newInstance("title", "message", extraPhoneNumber);
			fragment.show(getSupportFragmentManager(), CallsAlertDialogFragment.TAG);
		}
	}
}