package net.kokohadoko.instil.phonebook.content;

import net.kokohadoko.instil.android.content.SharedPreferencesHelper;
import net.kokohadoko.instil.phonebook.IncomingCall;
import net.kokohadoko.instil.phonebook.ReplyMessage;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.media.AudioManager;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Data;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;

/**
 * 電話着信などの状態を取得するためのBroadcastReceiverを構成するクラス
 * 
 * @author inuko
 * @since 0.0.1
 */
public class PhoneStateReceiver extends BroadcastReceiver {

	/** ログ出力用文字列 */
	public static final String LOG_TAG = PhoneStateReceiver.class.getSimpleName();

	/** 電話を表す文字列 */
	public static final String CALL = "call";
	/** SMSを表す文字列 */
	public static final String SMS = "sms";

	/** SharedPreferences内の着信状態を表す文字列 */
	private static final String PREF_KEY_PHONE_STATE = "phoneState";
	/** SharedPreferences内の着信音種別を表す文字列 */
	private static final String PREF_KEY_RINGER_MODE = "ringerMode";

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onReceive(Context context, Intent intent) {
	
		SharedPreferencesHelper pref = new SharedPreferencesHelper(context.getApplicationContext());

		final String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
		// 着信状態を保存
		Editor edit = pref.edit();
		edit.putString(PREF_KEY_PHONE_STATE, state);
		edit.commit();

		if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {

			/**
			 * 着信時の処理
			 */
			if (pref.getBoolean(CALL, false)) {
				final String number = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
				Cursor c = context.getContentResolver().query(
						ContactsContract.CommonDataKinds.Phone.CONTENT_URI, 
						new String[] { ContactsContract.Contacts.LOOKUP_KEY }, 
						Data.MIMETYPE + " = ? AND " + ContactsContract.CommonDataKinds.Phone.NUMBER + " = ?",
						new String[] { ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, number },
						Data.CONTACT_ID);

				if (c.isFirst()) {
					
					String lookup = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts.LOOKUP_KEY));
					Cursor c2 = context.getContentResolver().query(
							IncomingCall.CONTENT_URI, 
							null,
							IncomingCall.LOOKUP_KEY + " = ?",
							new String[] { lookup },
							null);
					if (c2.isFirst()) {
						final AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
						int ringerMode = audioManager.getRingerMode();

						edit.putInt(PREF_KEY_RINGER_MODE, ringerMode);
						edit.commit();

						audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
					}					
				}
			}			
		} else if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
	
			/**
			 * 通話終了時の処理
			 */
			
			final AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
			int ringerMode = pref.getInt(PREF_KEY_RINGER_MODE, audioManager.getRingerMode());
			audioManager.setRingerMode(ringerMode);

			// 自動応答設定が有効 かつ 不在着信
			if (pref.getBoolean(SMS, false) && pref.getString(PREF_KEY_PHONE_STATE, "").equals(TelephonyManager.CALL_STATE_RINGING)) {

				final String number = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
				Cursor c = context.getContentResolver().query(
						ContactsContract.CommonDataKinds.Phone.CONTENT_URI, 
						new String[] { ContactsContract.Contacts.LOOKUP_KEY }, 
						Data.MIMETYPE + " = ? AND " + ContactsContract.CommonDataKinds.Phone.NUMBER + " = ?",
						new String[] { ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, number },
						Data.CONTACT_ID);
				if (c.isFirst()) {
					String lookup = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts.LOOKUP_KEY));
					Cursor c2 = context.getContentResolver().query(
							ReplyMessage.CONTENT_URI, 
							new String[] { ReplyMessage.MESSAGE },
							ReplyMessage.LOOKUP_KEY + " = ?",
							new String[] { lookup },
							null);
					if (c2.isFirst()) {
						
						String message = c2.getString(c2.getColumnIndexOrThrow(ReplyMessage.MESSAGE));
						/**
						 * 通話終了時に該当する電話番号の場合対応するSMSを送信する
						 */
						SmsManager smsManager = SmsManager.getDefault();
						smsManager.sendTextMessage(number, null, message, null, null);
					}
				}
			}
		}
	}
}