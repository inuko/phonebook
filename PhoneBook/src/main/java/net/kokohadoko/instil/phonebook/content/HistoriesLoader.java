package net.kokohadoko.instil.phonebook.content;

import java.util.ArrayList;
import java.util.Iterator;

import net.kokohadoko.instil.database.CursorJoinerWithIntKey;
import net.kokohadoko.instil.database.CursorJoinerWithIntKey.Result;
import net.kokohadoko.instil.phonebook.CallLogs;
import net.kokohadoko.instil.phonebook.Calls;
import net.kokohadoko.instil.phonebook.Email;
import net.kokohadoko.instil.phonebook.Event;
import net.kokohadoko.instil.phonebook.Nickname;
import net.kokohadoko.instil.phonebook.PersonCalls;
import net.kokohadoko.instil.phonebook.Phone;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.RawContacts;
import android.support.v4.content.AsyncTaskLoader;

/**
 * グループ毎の着信履歴の一覧を取得するローダー
 * 
 * @author inuko
 * @since 0.0.1
 */
public class HistoriesLoader extends AsyncTaskLoader<Cursor> {

	/** ログ出力用文字列 */
	private static final String LOG_TAG = HistoriesLoader.class.getSimpleName();

	/** コンテキスト */
	private Context mContext;

	/** グループID */
	protected long groupId;
	/** アカウント名 */
	protected String accountName;
	/** アカウント種別 */
	protected String accountType;
	/** グループ名 */
	protected String title;
	/** 抽出条件 */
	protected String query;

	/**
	 * コンストラクタ
	 * 
	 * @since 0.0.1
	 * @param context コンテキスト
	 * @param groupId グループID
	 * @param accountName アカウント名
	 * @param accountType アカウント種別 
	 * @param title グループ名
	 * @param query 抽出条件
	 */
	public HistoriesLoader(Context context, long groupId, String accountName, String accountType, String title, String query) {
		super(context);

		this.mContext = context;

		this.groupId = groupId;
		this.accountName = accountName;
		this.accountType = accountType;
		this.title = title;
		this.query = query;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Cursor loadInBackground() {
	
		ArrayList<PersonCalls> personCallList = new ArrayList<PersonCalls>();
		
		MatrixCursor mc = new MatrixCursor(
				new String[] { 
						RawContacts._ID,
						Contacts.LOOKUP_KEY,
						Contacts.PHOTO_THUMBNAIL_URI,
						Contacts.LAST_TIME_CONTACTED,
						Contacts.CUSTOM_RINGTONE
				});
		
		/*
		MatrixCursor mc2 = new MatrixCursor(
				
				new String[] { 
						Fields.RAW_CONTACTS_ID,
						Fields.LOOKUP,
						Fields.PHOTO_THUMBNAIL_URI,
						Fields.HAS_PHONE_NUMBER,
						Fields.LAST_TIME_CONTACTED,
						Fields.CUSTOM_RINGTONE,
						Fields.DISPLAY_NAME,
						Fields.PHONETIC_NAME,
						Fields.NOTE,
						Fields.COMPANY,
						Fields.TITLE
				});*/
		// 
		MatrixCursor mc2 = new MatrixCursor(CallLogs.getColumnNames());

		Cursor contactsCursor = null;
		if (this.groupId != -1 && this.title.equals("My Contacts")) {
			contactsCursor = getContext().getContentResolver().query(
					Contacts.CONTENT_URI,
					null,
					Contacts.HAS_PHONE_NUMBER + " = ?",
					new String[] { String.valueOf(1) },
					Contacts._ID);
		} else {
			contactsCursor = getContext().getContentResolver().query(
					Uri.withAppendedPath(Contacts.CONTENT_GROUP_URI, this.title),
					null,
					Contacts.HAS_PHONE_NUMBER + " = ?",
					new String[] { String.valueOf(1) },
					Contacts._ID);
		}

		Cursor rawContactsCursor = getContext().getContentResolver().query(
				RawContacts.CONTENT_URI,
				null,
				RawContacts.DELETED + " = 0 AND " + RawContacts.ACCOUNT_NAME + " = ? AND " + RawContacts.ACCOUNT_TYPE + " = ?", 
				new String[] { this.accountName, this.accountType },
				RawContacts.CONTACT_ID);
		CursorJoinerWithIntKey joiner = new CursorJoinerWithIntKey(
				contactsCursor,
				new String[]{ Contacts._ID },
				rawContactsCursor,
				new String[] { RawContacts.CONTACT_ID });
		for (CursorJoinerWithIntKey.Result joinerResult : joiner) {
			if (joinerResult == Result.BOTH) {
				long id = rawContactsCursor.getLong(rawContactsCursor.getColumnIndexOrThrow(RawContacts._ID));
				String lookup = contactsCursor.getString(contactsCursor.getColumnIndexOrThrow(Contacts.LOOKUP_KEY));
				String photoThumbnailUri = contactsCursor.getString(contactsCursor.getColumnIndexOrThrow(Contacts.PHOTO_THUMBNAIL_URI));
				String lastTimeContacted = rawContactsCursor.getString(rawContactsCursor.getColumnIndexOrThrow(RawContacts.LAST_TIME_CONTACTED));
				String customRingtone = contactsCursor.getString(contactsCursor.getColumnIndexOrThrow(Contacts.CUSTOM_RINGTONE));
				mc.addRow(new Object[] {
						id,
						lookup,
						photoThumbnailUri,
						lastTimeContacted,
						customRingtone
				});
			}
		}

		while (mc.moveToNext()) {
			final long id = mc.getLong(mc.getColumnIndexOrThrow(RawContacts._ID));
			final String lookup = mc.getString(mc.getColumnIndexOrThrow(Contacts.LOOKUP_KEY));
			final String photoThumbnailUri = mc.getString(mc.getColumnIndexOrThrow(Contacts.PHOTO_THUMBNAIL_URI));
			final String lastTimeContacted = mc.getString(mc.getColumnIndexOrThrow(RawContacts.LAST_TIME_CONTACTED));
			final String customRingtone = mc.getString(mc.getColumnIndexOrThrow(Contacts.CUSTOM_RINGTONE));

			Uri uri1 = ContentUris.withAppendedId(RawContacts.CONTENT_URI, id);
			Uri uri2 = Uri.withAppendedPath(uri1, RawContacts.Entity.CONTENT_DIRECTORY);

			PersonCalls personCalls = new PersonCalls(id, lookup, photoThumbnailUri, 1, lastTimeContacted, customRingtone);

			Cursor c = getContext().getContentResolver().query(
					uri2,
					null,
					null,
					null,
					null);
			while (c.moveToNext()) {
				final String mimetype = c.getString(c.getColumnIndexOrThrow(Data.MIMETYPE));

				if (mimetype.equals(ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)) {
					String displayName = c.getString(c.getColumnIndexOrThrow(StructuredName.DISPLAY_NAME));
					String phoneticFamilyName = c.getString(c.getColumnIndexOrThrow(StructuredName.PHONETIC_FAMILY_NAME));
					String phoneticMiddleName = c.getString(c.getColumnIndexOrThrow(StructuredName.PHONETIC_MIDDLE_NAME));
					String phoneticGivenName = c.getString(c.getColumnIndexOrThrow(StructuredName.PHONETIC_GIVEN_NAME));

					personCalls.setDisplayName(displayName);
					personCalls.setPhoneticName(phoneticFamilyName, phoneticMiddleName, phoneticGivenName);
					
				//} else if (mimetype.equals(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)) {
					
					
					
				} else if (mimetype.equals(ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)) {
					
					String address = c.getString(c.getColumnIndexOrThrow(CommonDataKinds.Email.ADDRESS));
					int type = c.getInt(c.getColumnIndexOrThrow(CommonDataKinds.Email.TYPE));
					Email email = new Email(address, type);
					personCalls.addEmail(email);
					
				} else if (mimetype.equals(ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)) {

					String number = c.getString(c.getColumnIndexOrThrow(CommonDataKinds.Phone.NUMBER));
					if (number != null) {
						number = number.replaceAll("-", "");
					}
					String label = c.getString(c.getColumnIndexOrThrow(CommonDataKinds.Phone.LABEL));
					int type = c.getInt(c.getColumnIndexOrThrow(CommonDataKinds.Phone.TYPE));
					Phone phone = new Phone(number, type, label);
					personCalls.addPhone(phone);

				} else if (mimetype.equals(ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE)) {
					
					String company = c.getString(c.getColumnIndexOrThrow(CommonDataKinds.Organization.COMPANY));
					String title = c.getString(c.getColumnIndexOrThrow(CommonDataKinds.Organization.TITLE));
					
					personCalls.setCompany(company);
					personCalls.setTitle(title);
					
				//} else if (mimetype.equals(ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE)) {
					
				} else if (mimetype.equals(ContactsContract.CommonDataKinds.Nickname.CONTENT_ITEM_TYPE)) {

					String name = c.getString(c.getColumnIndexOrThrow(CommonDataKinds.Nickname.NAME));
					String label = c.getString(c.getColumnIndexOrThrow(CommonDataKinds.Nickname.LABEL));
					int type = c.getInt(c.getColumnIndexOrThrow(CommonDataKinds.Nickname.TYPE));

					Nickname nickname = new Nickname(name, label, type);
					personCalls.setNickname(nickname);

				} else if (mimetype.equals(ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE)) {
					
					String note = c.getString(c.getColumnIndexOrThrow(CommonDataKinds.Note.NOTE));
					personCalls.setNote(note);

				//} else if (mimetype.equals(ContactsContract.CommonDataKinds.GroupMembership.CONTENT_ITEM_TYPE)) {
					
				//} else if (mimetype.equals(ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE)) {
					
				} else if (mimetype.equals(ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE)) {
					
					String label = c.getString(c.getColumnIndexOrThrow(CommonDataKinds.Event.LABEL));
					int type = c.getInt(c.getColumnIndexOrThrow(CommonDataKinds.Event.TYPE));
					Event event = new Event(label, type);
					personCalls.addEvent(event);
					
				} else if (mimetype.equals(ContactsContract.CommonDataKinds.Relation.CONTENT_ITEM_TYPE)) {
					
				//} else if (mimetype.equals(ContactsContract.CommonDataKinds.SipAddress.CONTENT_ITEM_TYPE)) {
					
				}
			}

			// 
			if (PersonCalls.isMatch(mContext, personCalls, query)) {
				//mc2.addRow(personCalls.toObjects());
				personCallList.add(personCalls);
			}
		}

		Cursor callLogsCursor = getContext().getContentResolver().query(
				CallLog.Calls.CONTENT_URI,
				new String[] { CallLog.Calls.NUMBER, CallLog.Calls.DATE, CallLog.Calls.DURATION, CallLog.Calls.TYPE },
				null,
				null,
				CallLog.Calls.DEFAULT_SORT_ORDER);
		while (callLogsCursor.moveToNext()) {
			// 
			Calls calls = new Calls(callLogsCursor);
			final String number = calls.getNumber();

			Iterator<PersonCalls> itr = personCallList.iterator();
			while (itr.hasNext()) {
				PersonCalls personCalls = itr.next();
				if (PersonCalls.isMatchesNumber(personCalls, number)) {
					CallLogs callLogs = new CallLogs(
							personCalls.getRawContactsId(), personCalls.getLookup(),
							personCalls.getPhotoThumbnailUri(), personCalls.getDisplayName(),
							number, calls.getDate(), calls.getDuration(), calls.getType());
					mc2.addRow(callLogs.toObjects());
				}
			}
		}

		return mc2;
/*
		while (cc.moveToNext()) {
			String number = cc.getString(cc.getColumnIndexOrThrow(CallLog.Calls.NUMBER));
			long date = cc.getLong(cc.getColumnIndexOrThrow(CallLog.Calls.DATE));
			long duration = cc.getLong(cc.getColumnIndexOrThrow(CallLog.Calls.DURATION));
			int type = cc.getInt(cc.getColumnIndexOrThrow(CallLog.Calls.TYPE));

			if (mc3.moveToFirst()) {
				do {
					
					String _number = mc.getString(mc3.getColumnIndexOrThrow(CommonDataKinds.Phone.NUMBER));
					if (_number != null && number.equals(_number)) {
						
						long contactId = mc3.getLong(mc3.getColumnIndexOrThrow(Contacts._ID));
						String displayNamePrimary = mc3.getString(mc3.getColumnIndexOrThrow(Contacts.DISPLAY_NAME_PRIMARY));
						String lookupKey = mc3.getString(mc3.getColumnIndexOrThrow(Contacts.LOOKUP_KEY));
						String photoThumbnailUri = mc3.getString(mc3.getColumnIndexOrThrow(Contacts.PHOTO_THUMBNAIL_URI));
				
						mc2.addRow(new Object[] {
							contactId,
							displayNamePrimary,
							lookupKey,
							photoThumbnailUri,
							number,
							date,
							duration,
							type
						});
					}
				} while (mc.moveToNext());
			}
		}

		CursorJoinerWithIntKey joiner = new CursorJoinerWithIntKey(cc, new String[]{  }, cc2, new String[] { Data.CONTACT_ID });
		for (CursorJoinerWithIntKey.Result joinerResult : joiner) {
			if (joinerResult == Result.BOTH) {
		
		
		// TODO:履歴とDataを統合する
		// TODO:各種ユーザを取得する
		
		
		// 
		MatrixCursor mc = new MatrixCursor(
			new String[] { 
				Contacts._ID,
				Contacts.DISPLAY_NAME_PRIMARY,
				Contacts.LOOKUP_KEY, 
				Contacts.PHOTO_THUMBNAIL_URI,
			}
		);
				
		Uri uri = Uri.withAppendedPath(Contacts.CONTENT_GROUP_URI, this.title);
		
		Cursor c = getContext().getContentResolver().query(uri, null, null, null, null);
		
		if (query != null) {
			Cursor c2 = getContext().getContentResolver().query(
					Data.CONTENT_URI,
					null,
					Data.MIMETYPE + " = ? AND " + 
					StructuredName.PHONETIC_NAME + " LIKE '%' || ? || '%' ESCAPE '$'",
				//	StructuredName.PHONETIC_FAMILY_NAME + " LIKE '%' || ? || '%' ESCAPE '$' OR " + 
				//	StructuredName.PHONETIC_GIVEN_NAME + " LIKE '%' || ? || '%' ESCAPE '$' OR "  + 
				//	StructuredName.PHONETIC_MIDDLE_NAME + " LIKE '%' || ? || '%' ESCAPE '$' OR "
				//  + StructuredName.DISPLAY_NAME + " LIKE '%' || ? || '%' ESCAPE '$')",
					new String[] { StructuredName.CONTENT_ITEM_TYPE, query },
					Data.CONTACT_ID);
			
			CursorJoinerWithIntKey joiner = new CursorJoinerWithIntKey(c, new String[]{ Contacts._ID }, c2, new String[] { Data.CONTACT_ID });
			for (CursorJoinerWithIntKey.Result joinerResult : joiner) {
				if (joinerResult == Result.BOTH) {
					long contactId = c.getLong(c.getColumnIndexOrThrow(Contacts._ID));
					String displayName = c.getString(c.getColumnIndexOrThrow(Contacts.DISPLAY_NAME_PRIMARY));
					String lookupKey = c.getString(c.getColumnIndexOrThrow(Contacts.LOOKUP_KEY));
					String photoThumbnailUri = c.getString(c.getColumnIndexOrThrow(Contacts.PHOTO_THUMBNAIL_URI));
					
					mc.addRow(new Object[] { contactId, displayName, lookupKey, photoThumbnailUri });
				}
			}
			
			Cursor c3 = getContext().getContentResolver().query(uri, null, Contacts.DISPLAY_NAME_PRIMARY + " LIKE '%' || ? || '%' ESCAPE '$'", new String[] { query }, Contacts._ID);
			
			CursorJoinerWithIntKey joiner2 = new CursorJoinerWithIntKey(mc, new String[]{ Contacts._ID }, c3, new String[] { Contacts._ID });
			for (CursorJoinerWithIntKey.Result joinerResult : joiner2) {
				if (joinerResult == Result.RIGHT) {
					long contactId = c3.getLong(c.getColumnIndexOrThrow(Contacts._ID));
					String displayName = c3.getString(c.getColumnIndexOrThrow(Contacts.DISPLAY_NAME_PRIMARY));
					String lookupKey = c3.getString(c.getColumnIndexOrThrow(Contacts.LOOKUP_KEY));
					String photoThumbnailUri = c3.getString(c.getColumnIndexOrThrow(Contacts.PHOTO_THUMBNAIL_URI));
					
					mc.addRow(new Object[] { contactId, displayName, lookupKey, photoThumbnailUri });
				}
			}
			
		} else {
			while (c.moveToNext()) {
				long contactId = c.getLong(c.getColumnIndexOrThrow(Contacts._ID));
				String displayName = c.getString(c.getColumnIndexOrThrow(Contacts.DISPLAY_NAME_PRIMARY));
				String lookupKey = c.getString(c.getColumnIndexOrThrow(Contacts.LOOKUP_KEY));
				String photoThumbnailUri = c.getString(c.getColumnIndexOrThrow(Contacts.PHOTO_THUMBNAIL_URI));
					
				mc.addRow(new Object[] { contactId, displayName, lookupKey, photoThumbnailUri });
			}
		}
		
		
		// 
				MatrixCursor mc3 = new MatrixCursor(
						new String[] { 
								Contacts._ID,
								Contacts.DISPLAY_NAME_PRIMARY,
								Contacts.LOOKUP_KEY, 
								Contacts.PHOTO_THUMBNAIL_URI,
								CommonDataKinds.Phone.NUMBER,
								CommonDataKinds.Phone.NORMALIZED_NUMBER
						}
				);
				// 
				MatrixCursor mc2 = new MatrixCursor(
						new String[] { 
								Contacts._ID,
								Contacts.DISPLAY_NAME_PRIMARY,
								Contacts.LOOKUP_KEY, 
								Contacts.PHOTO_THUMBNAIL_URI,
								CallLog.Calls.NUMBER,
								CallLog.Calls.DATE,
								CallLog.Calls.DURATION,
								CallLog.Calls.TYPE
						}
				);
		
		
		
		
		
		Cursor c2 = getContext().getContentResolver().query(
				Data.CONTENT_URI,
				null,
				Data.MIMETYPE + " = ?",
				new String[] { CommonDataKinds.Phone.CONTENT_ITEM_TYPE },
				Data.CONTACT_ID);

		CursorJoinerWithIntKey cj = new CursorJoinerWithIntKey(mc, new String[] { Contacts._ID }, c2, new String[] { Data.CONTACT_ID });
		for (CursorJoinerWithIntKey.Result result : cj) {
			switch (result) {
				case BOTH:
				{
					long contactId = mc.getLong(mc.getColumnIndexOrThrow(Contacts._ID));
					String displayName = mc.getString(mc.getColumnIndexOrThrow(Contacts.DISPLAY_NAME_PRIMARY));
					String lookupKey = mc.getString(mc.getColumnIndexOrThrow(Contacts.LOOKUP_KEY));
					String photoThumbnailUri = mc.getString(mc.getColumnIndexOrThrow(Contacts.PHOTO_THUMBNAIL_URI));
					
					String number = c2.getString(c2.getColumnIndexOrThrow(CommonDataKinds.Phone.NUMBER)).replaceAll("-", "");
					String normalizedNumber = c2.getString(c2.getColumnIndexOrThrow(CommonDataKinds.Phone.NORMALIZED_NUMBER));

					mc3.addRow(new Object[] {
						contactId,
						displayName,
						lookupKey,
						photoThumbnailUri,
						number,
						normalizedNumber
					});
				}
					break;

				default:
					break;
			}
		}

		Cursor c3 = getContext().getContentResolver().query(
				CallLog.Calls.CONTENT_URI,
				new String[] { CallLog.Calls.NUMBER, CallLog.Calls.DATE, CallLog.Calls.DURATION, CallLog.Calls.TYPE },
				null,
				null,
				CallLog.Calls.DEFAULT_SORT_ORDER);
		
		if (c3.moveToFirst()) {
			do {
				String number = c3.getString(c3.getColumnIndexOrThrow(CallLog.Calls.NUMBER));
				long date = c3.getLong(c3.getColumnIndexOrThrow(CallLog.Calls.DATE));
				long duration = c3.getLong(c3.getColumnIndexOrThrow(CallLog.Calls.DURATION));
				int type = c3.getInt(c3.getColumnIndexOrThrow(CallLog.Calls.TYPE));

				if (mc3.moveToFirst()) {
					do {
						
						String _number = mc.getString(mc3.getColumnIndexOrThrow(CommonDataKinds.Phone.NUMBER));
						if (_number != null && number.equals(_number)) {
							
							long contactId = mc3.getLong(mc3.getColumnIndexOrThrow(Contacts._ID));
							String displayNamePrimary = mc3.getString(mc3.getColumnIndexOrThrow(Contacts.DISPLAY_NAME_PRIMARY));
							String lookupKey = mc3.getString(mc3.getColumnIndexOrThrow(Contacts.LOOKUP_KEY));
							String photoThumbnailUri = mc3.getString(mc3.getColumnIndexOrThrow(Contacts.PHOTO_THUMBNAIL_URI));
					
							mc2.addRow(new Object[] {
								contactId,
								displayNamePrimary,
								lookupKey,
								photoThumbnailUri,
								number,
								date,
								duration,
								type
							});
						}
					} while (mc.moveToNext());
				}
			} while (c3.moveToNext());
		}
		*/
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onStartLoading() {
		super.onStartLoading();
	
		forceLoad();
	}
}