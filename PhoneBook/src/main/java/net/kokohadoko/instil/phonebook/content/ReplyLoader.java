package net.kokohadoko.instil.phonebook.content;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.provider.ContactsContract.Contacts;
import android.support.v4.content.AsyncTaskLoader;

/**
 * 不在着信があった場合の自動返信メッセージの一覧を取得する
 * 
 * @author inuko
 * @since 0.0.1
 */
public class ReplyLoader extends AsyncTaskLoader<Cursor> {

	/**
	 * コンストラクタ
	 * 
	 * @since 0.0.1
	 * @param context コンテキスト
	 */
	public ReplyLoader(Context context) {
		super(context);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Cursor loadInBackground() {
		MatrixCursor mc = new MatrixCursor(
				new String [] {
						Contacts._ID,
						Contacts.DISPLAY_NAME_PRIMARY,
						Contacts.LOOKUP_KEY, 
						Contacts.PHOTO_THUMBNAIL_URI
				}
		);
		
		return mc;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onStartLoading() {
		super.onStartLoading();
	
		forceLoad();
	}
}