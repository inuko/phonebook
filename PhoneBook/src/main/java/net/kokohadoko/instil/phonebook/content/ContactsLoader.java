package net.kokohadoko.instil.phonebook.content;

import java.util.HashMap;

import net.kokohadoko.instil.android.content.SharedPreferencesHelper;
import net.kokohadoko.instil.database.CursorJoinerWithIntKey;
import net.kokohadoko.instil.database.CursorJoinerWithIntKey.Result;
import net.kokohadoko.instil.phonebook.Email;
import net.kokohadoko.instil.phonebook.Event;
import net.kokohadoko.instil.phonebook.Fields;
import net.kokohadoko.instil.phonebook.Nickname;
import net.kokohadoko.instil.phonebook.Person;
import net.kokohadoko.instil.phonebook.Phone;
import net.kokohadoko.instil.phonebook.SearchableFields;
import net.kokohadoko.instil.phonebook.VisibleFields;
import android.R.integer;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.provider.Contacts.GroupMembership;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Im;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.CommonDataKinds.Website;
import android.provider.ContactsContract.CommonDataKinds;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.RawContacts;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

/**
 * グループ毎の連絡先の一覧を取得する
 * 
 * @author inuko
 * @since 0.0.1
 */
public class ContactsLoader extends AsyncTaskLoader<Cursor> {

	/** ログ出力用文字列 */
	private static final String LOG_TAG = ContactsLoader.class.getSimpleName();
	/** 端末内の着信音一覧のキャッシュ用 */
	private static final HashMap<String, String> map = new HashMap<String, String>();

	/** RingtoneManager */
	private RingtoneManager mRingtoneManager;
	
	/** コンテキスト */
	private Context mContext;

	/** グループID */
	protected long groupId;
	/** アカウント名 */
	protected String accountName;
	/** アカウント種別 */
	protected String accountType;
	/** グループ名 */
	protected String title;
	/** 抽出条件 */
	protected String query;

	/**
	 * コンストラクタ
	 * 
	 * @since 0.0.1
	 * @param context コンテキスト
	 * @param groupId グループID
	 * @param accountName アカウント名
	 * @param accountType アカウント種別 
	 * @param title グループ名
	 * @param query 
	 */
	public ContactsLoader(Context context, long groupId, String accountName, String accountType, String title, String query) {
		super(context);
		
		mRingtoneManager = new RingtoneManager(context);
		mContext = context;
		
		this.groupId = groupId;
		this.accountName = accountName;
		this.accountType = accountType;
		this.title = title;
		this.query = query;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Cursor loadInBackground() {

		MatrixCursor mc4 = new MatrixCursor(
				new String[] { 
						RawContacts._ID,
						Contacts.LOOKUP_KEY,
						Contacts.PHOTO_THUMBNAIL_URI,
						Contacts.HAS_PHONE_NUMBER,
						Contacts.LAST_TIME_CONTACTED,
						Contacts.CUSTOM_RINGTONE
				});
		MatrixCursor mc5 = new MatrixCursor(
				new String[] {
						Fields.RAW_CONTACTS_ID,
						Fields.LOOKUP,
						Fields.PHOTO_THUMBNAIL_URI,
						Fields.HAS_PHONE_NUMBER,
						Fields.LAST_TIME_CONTACTED,
						Fields.CUSTOM_RINGTONE,
						Fields.DISPLAY_NAME,
						Fields.PHONETIC_NAME,
						Fields.NOTE,
						Fields.COMPANY,
						Fields.TITLE
				});

		Cursor cc = null;

		if (this.groupId != -1 && this.title.equals("My Contacts")) {
			cc = getContext().getContentResolver().query(
					Contacts.CONTENT_URI,
					new String[] { Contacts._ID, Contacts.LOOKUP_KEY, Contacts.PHOTO_THUMBNAIL_URI, Contacts.HAS_PHONE_NUMBER, Contacts.CUSTOM_RINGTONE },
					null,
					null,
					Contacts._ID);
		} else {
			cc = getContext().getContentResolver().query(
					Uri.withAppendedPath(Contacts.CONTENT_GROUP_URI, this.title),
					new String[] { Contacts._ID, Contacts.LOOKUP_KEY, Contacts.PHOTO_THUMBNAIL_URI, Contacts.HAS_PHONE_NUMBER, Contacts.CUSTOM_RINGTONE },
					null,
					null,
					Contacts._ID);
		}

		Cursor cc2 = getContext().getContentResolver().query(
				RawContacts.CONTENT_URI,
				null,
				RawContacts.DELETED + " = 0 AND " + RawContacts.ACCOUNT_NAME + " = ? AND " + RawContacts.ACCOUNT_TYPE + " = ?", 
				new String[] { this.accountName, this.accountType },
				RawContacts.CONTACT_ID);
		CursorJoinerWithIntKey joiner3 = new CursorJoinerWithIntKey(
				cc, 
				new String[]{ Contacts._ID },
				cc2, 
				new String[] { RawContacts.CONTACT_ID });
		
		
		
		for (CursorJoinerWithIntKey.Result joinerResult : joiner3) {
			if (joinerResult == Result.BOTH) {
				long id = cc2.getLong(cc2.getColumnIndexOrThrow(RawContacts._ID));
				String lookup = cc.getString(cc.getColumnIndexOrThrow(Contacts.LOOKUP_KEY));
				String photoThumbnailUri = cc.getString(cc.getColumnIndexOrThrow(Contacts.PHOTO_THUMBNAIL_URI));
				int hasPhoneNumber = cc.getInt(cc.getColumnIndexOrThrow(Contacts.HAS_PHONE_NUMBER));
				String lastTimeContacted = cc2.getString(cc2.getColumnIndexOrThrow(RawContacts.LAST_TIME_CONTACTED));
				String customRingtone = cc.getString(cc.getColumnIndexOrThrow(Contacts.CUSTOM_RINGTONE));
				mc4.addRow(new Object[] {
						id,
						lookup,
						photoThumbnailUri,
						hasPhoneNumber,
						lastTimeContacted,
						customRingtone
				});
			}
		}

		// 電話帳１件ごとの処理
		while (mc4.moveToNext()) {
			final long id = mc4.getLong(mc4.getColumnIndexOrThrow(RawContacts._ID));
			final String lookup = mc4.getString(mc4.getColumnIndexOrThrow(Contacts.LOOKUP_KEY));
			final String photoThumbnailUri = mc4.getString(mc4.getColumnIndexOrThrow(Contacts.PHOTO_THUMBNAIL_URI));
			final int hasPhoneNumber = mc4.getInt(mc4.getColumnIndexOrThrow(Contacts.HAS_PHONE_NUMBER));
			final String lastTimeContacted = mc4.getString(mc4.getColumnIndexOrThrow(RawContacts.LAST_TIME_CONTACTED));
			final String customRingtone = mc4.getString(mc4.getColumnIndexOrThrow(Contacts.CUSTOM_RINGTONE));

			Uri uri1 = ContentUris.withAppendedId(RawContacts.CONTENT_URI, id);
			Uri uri2 = Uri.withAppendedPath(uri1, RawContacts.Entity.CONTENT_DIRECTORY);

			Person person = new Person(id, lookup, photoThumbnailUri, hasPhoneNumber, lastTimeContacted, customRingtone);

			Cursor c = getContext().getContentResolver().query(
					uri2,
					null,
					null,
					null,
					null);
			while (c.moveToNext()) {
				final String mimetype = c.getString(c.getColumnIndexOrThrow(Data.MIMETYPE));

				if (mimetype.equals(ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)) {
					String displayName = c.getString(c.getColumnIndexOrThrow(StructuredName.DISPLAY_NAME));
					String phoneticFamilyName = c.getString(c.getColumnIndexOrThrow(StructuredName.PHONETIC_FAMILY_NAME));
					String phoneticMiddleName = c.getString(c.getColumnIndexOrThrow(StructuredName.PHONETIC_MIDDLE_NAME));
					String phoneticGivenName = c.getString(c.getColumnIndexOrThrow(StructuredName.PHONETIC_GIVEN_NAME));

					person.setDisplayName(displayName);
					person.setPhoneticName(phoneticFamilyName, phoneticMiddleName, phoneticGivenName);
					
				//} else if (mimetype.equals(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)) {
					
					
					
				} else if (mimetype.equals(ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)) {
					
					String address = c.getString(c.getColumnIndexOrThrow(CommonDataKinds.Email.ADDRESS));
					int type = c.getInt(c.getColumnIndexOrThrow(CommonDataKinds.Email.TYPE));
					Email email = new Email(address, type);
					person.addEmail(email);
					
				} else if (mimetype.equals(ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)) {

					String number = c.getString(c.getColumnIndexOrThrow(CommonDataKinds.Phone.NUMBER));
					String label = c.getString(c.getColumnIndexOrThrow(CommonDataKinds.Phone.LABEL));
					int type = c.getInt(c.getColumnIndexOrThrow(CommonDataKinds.Phone.TYPE));
					Phone phone = new Phone(number, type, label);
					person.addPhone(phone);

				} else if (mimetype.equals(ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE)) {
					
					String company = c.getString(c.getColumnIndexOrThrow(CommonDataKinds.Organization.COMPANY));
					String title = c.getString(c.getColumnIndexOrThrow(CommonDataKinds.Organization.TITLE));
					
					person.setCompany(company);
					person.setTitle(title);
					
				//} else if (mimetype.equals(ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE)) {
					
				} else if (mimetype.equals(ContactsContract.CommonDataKinds.Nickname.CONTENT_ITEM_TYPE)) {

					String name = c.getString(c.getColumnIndexOrThrow(CommonDataKinds.Nickname.NAME));
					String label = c.getString(c.getColumnIndexOrThrow(CommonDataKinds.Nickname.LABEL));
					int type = c.getInt(c.getColumnIndexOrThrow(CommonDataKinds.Nickname.TYPE));

					Nickname nickname = new Nickname(name, label, type);
					person.setNickname(nickname);

				} else if (mimetype.equals(ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE)) {
					
					String note = c.getString(c.getColumnIndexOrThrow(CommonDataKinds.Note.NOTE));
					person.setNote(note);

				//} else if (mimetype.equals(ContactsContract.CommonDataKinds.GroupMembership.CONTENT_ITEM_TYPE)) {
					
				//} else if (mimetype.equals(ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE)) {
					
				} else if (mimetype.equals(ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE)) {
					
					String label = c.getString(c.getColumnIndexOrThrow(CommonDataKinds.Event.LABEL));
					int type = c.getInt(c.getColumnIndexOrThrow(CommonDataKinds.Event.TYPE));
					Event event = new Event(label, type);
					person.addEvent(event);
					
				} else if (mimetype.equals(ContactsContract.CommonDataKinds.Relation.CONTENT_ITEM_TYPE)) {
					
				//} else if (mimetype.equals(ContactsContract.CommonDataKinds.SipAddress.CONTENT_ITEM_TYPE)) {
					
				}
			}

			// 
			if (Person.isMatch(mContext, person, query)) {
				mc5.addRow(person.toObjects());
			}
		}

		Uri defaultUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
		Ringtone defaultRingtone = RingtoneManager.getRingtone(getContext(), defaultUri);
		String defaultRingtoneTitle = defaultRingtone.getTitle(getContext());
		
		// 
		Cursor cursor = mRingtoneManager.getCursor();
		while (cursor.moveToNext()) {
			long id = cursor.getLong(RingtoneManager.ID_COLUMN_INDEX);
			String title = cursor.getString(RingtoneManager.TITLE_COLUMN_INDEX);
			String uriString = cursor.getString(RingtoneManager.URI_COLUMN_INDEX);

			Uri uri = Uri.withAppendedPath(Uri.parse(uriString), String.valueOf(id));
			Log.i(LOG_TAG, "TITLE:" + title);
			Log.i(LOG_TAG, "URI:" + uriString);
			
			map.put(uri.toString(), title);
		}

		
		// TODO:return 
		//return mc5;
		
/*
		Cursor c = null;
		Cursor phoneticCursor = null;

		if (this.groupId != -1 && this.title.equals("My Contacts")) {
			
			c = getContext().getContentResolver().query(
					Contacts.CONTENT_URI,
					null,
					null,
					null,
					Contacts._ID);
			while (c.moveToNext()) {
				boolean is = false;
				
				// TODO:ACCOUNT_TYPE, ACCOUNT_NAMEで抽出したものからrawContactIdを取得する
				long rawContactsId = 0;
				Uri uri1 = ContentUris.withAppendedId(RawContacts.CONTENT_URI, rawContactsId);
				Uri uri2 = Uri.withAppendedPath(uri1, ContactsContract.Contacts.Entity.CONTENT_DIRECTORY);
				// TODO:uri2から抽出される各データに対して
				
				c.getString(c.getColumnIndexOrThrow(Contacts.LOOKUP_KEY));
				String displayNamePrimary = c.getString(c.getColumnIndexOrThrow(Contacts.DISPLAY_NAME_PRIMARY));
				String customRingtone = c.getString(c.getColumnIndexOrThrow(Contacts.CUSTOM_RINGTONE));
				String phoneticName = null;
			
				// 表示名
				
				// ふりがな
				if (pref.getBoolean(VisibleFields.PHONETIC_NAME, false)) {
					if (pref.getBoolean(SearchableFields.PHONETIC_NAME, false) && query != null) {
						phoneticCursor = getContext().getContentResolver().query(
								Data.CONTENT_URI,
								new String[] { Data.CONTACT_ID, StructuredName.PHONETIC_NAME },
								Data.MIMETYPE + " = ? AND " + StructuredName.PHONETIC_NAME + " LIKE '%' || ? || '%' ESCAPE '$'",
								new String[] { StructuredName.CONTENT_ITEM_TYPE, query },
								Data.CONTACT_ID);
						while (phoneticCursor.moveToNext()) {
							phoneticName = phoneticCursor.getString(phoneticCursor.getColumnIndexOrThrow(StructuredName.PHONETIC_NAME));
						}
					} else {
						phoneticCursor = getContext().getContentResolver().query(
								Data.CONTENT_URI,
								new String[] { Data.CONTACT_ID, StructuredName.PHONETIC_NAME },
								Data.MIMETYPE + " = ?",
								new String[] { StructuredName.CONTENT_ITEM_TYPE, query },
								Data.CONTACT_ID);
						while (phoneticCursor.moveToNext()) {
							
						}
					}
				} else {
					if (pref.getBoolean(SearchableFields.PHONETIC_NAME, false) && query != null) {
						phoneticCursor = getContext().getContentResolver().query(
								Data.CONTENT_URI,
								new String[] { Data.CONTACT_ID, StructuredName.PHONETIC_NAME },
								Data.MIMETYPE + " = ? AND " + StructuredName.PHONETIC_NAME + " LIKE '%' || ? || '%' ESCAPE '$'",
								new String[] { StructuredName.CONTENT_ITEM_TYPE, query },
								Data.CONTACT_ID);
						while (phoneticCursor.moveToNext()) {
							
						}
					}
				}
			}
			
		} else {
			
		}
*/
		// 
		MatrixCursor mc = new MatrixCursor(
				new String[] { 
						Contacts._ID,
						Contacts.DISPLAY_NAME_PRIMARY,
						Contacts.LOOKUP_KEY, 
						Contacts.PHOTO_THUMBNAIL_URI,
						Contacts.CUSTOM_RINGTONE,
						Contacts.HAS_PHONE_NUMBER,
						Contacts.LAST_TIME_CONTACTED
				}
		);
/*
		Cursor displayNamePrimaryCursor = null;

		if (pref.getBoolean(VisibleFields.DISPLAY_NAME, false)) {
			if (pref.getBoolean(SearchableFields.DISPLAY_NAME, false) && query != null) {
				// TODO:指定検索
				
			} else {
				// TODO:全部検索
				
			}
		} else {
			if (pref.getBoolean(SearchableFields.DISPLAY_NAME, false) && query != null) {
				// TODO:指定検索
			} else {
				displayNamePrimaryCursor = null;
			}
		}
		

		if (query != null) {
			
			Cursor displayNamePrimaryCursor = null;
			Cursor phoneticCursor = null;
			Cursor customRingtoneCursor = null;
			
			// 表示名
			if (pref.getBoolean(SearchableFields.DISPLAY_NAME, false)) {
				displayNamePrimaryCursor = getContext().getContentResolver().query(
							ContactsContract.Contacts.CONTENT_URI, 
							null, 
							Contacts.DISPLAY_NAME_PRIMARY + " LIKE '%' || ? || '%' ESCAPE '$'", 
							new String[] { query },
							Contacts._ID);
			}
			// ふりがな
			if (pref.getBoolean(SearchableFields.PHONETIC_NAME, false)) {
				if (query != null) {
					phoneticCursor = getContext().getContentResolver().query(
							Data.CONTENT_URI,
							new String[] { Data.CONTACT_ID, StructuredName.PHONETIC_NAME },
							Data.MIMETYPE + " = ? AND " + StructuredName.PHONETIC_NAME + " LIKE '%' || ? || '%' ESCAPE '$'",
							new String[] { StructuredName.CONTENT_ITEM_TYPE, query },
							Data.CONTACT_ID);
				} else {
					phoneticCursor = getContext().getContentResolver().query(
							Data.CONTENT_URI,
							new String[] { Data.CONTACT_ID, StructuredName.PHONETIC_NAME },
							Data.MIMETYPE + " = ?",
							new String[] { StructuredName.CONTENT_ITEM_TYPE },
							Data.CONTACT_ID);
				}
			}
			// 着信音
			if (pref.getBoolean(SearchableFields.CUSTOM_RINGTONE, false)) {
				customRingtoneCursor = getContext().getContentResolver().query(
							ContactsContract.Contacts.CONTENT_URI, 
							null, 
							Contacts.CUSTOM_RINGTONE + " LIKE '%' || ? || '%' ESCAPE '$'", 
							new String[] { query }, 
							Contacts._ID);
			}
			
			if (this.groupId == -1 && this.title.equals("My Contacts")) {
				
			} else {
				Uri uri = Uri.withAppendedPath(Contacts.CONTENT_GROUP_URI, this.title);
				
			}
		} else {
			if (this.groupId == -1 && this.title.equals("My Contacts")) {
				Cursor contactsCursor = getContext().getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, Contacts._ID);
				Cursor rawContactsCursor = getContext().getContentResolver().query(
						RawContacts.CONTENT_URI,
						null,
						RawContacts.DELETED + " = 0 AND " + RawContacts.ACCOUNT_NAME + " = ? AND " + RawContacts.ACCOUNT_TYPE + " = ?", 
						new String[] { this.accountName, this.accountType },
						RawContacts.CONTACT_ID);

				
				CursorJoinerWithIntKey joiner = new CursorJoinerWithIntKey(contactsCursor, new String[]{ ContactsContract.Contacts._ID }, rawContactsCursor, new String[] { RawContacts.CONTACT_ID });
				for (CursorJoinerWithIntKey.Result joinerResult : joiner) {
					if (joinerResult == Result.BOTH) {
						long contactId = contactsCursor.getLong(contactsCursor.getColumnIndexOrThrow(Contacts._ID));
						String displayName = contactsCursor.getString(contactsCursor.getColumnIndexOrThrow(Contacts.DISPLAY_NAME_PRIMARY));
						String lookupKey = contactsCursor.getString(contactsCursor.getColumnIndexOrThrow(Contacts.LOOKUP_KEY));
						String photoThumbnailUri = contactsCursor.getString(contactsCursor.getColumnIndexOrThrow(Contacts.PHOTO_THUMBNAIL_URI));
						String customRingtone = contactsCursor.getString(contactsCursor.getColumnIndexOrThrow(Contacts.CUSTOM_RINGTONE));	
						int hasPhoneNumber = contactsCursor.getInt(contactsCursor.getColumnIndexOrThrow(Contacts.HAS_PHONE_NUMBER));
						int lastTimeContacted = contactsCursor.getInt(contactsCursor.getColumnIndexOrThrow(Contacts.LAST_TIME_CONTACTED));
						
						// Ringtone
						if (customRingtone != null) {
							String customRingtoneTitle = map.get(customRingtone);
							mc.addRow(new Object[] { contactId, displayName, lookupKey, photoThumbnailUri, customRingtoneTitle, hasPhoneNumber, lastTimeContacted });
						} else {
							mc.addRow(new Object[] { contactId, displayName, lookupKey, photoThumbnailUri, defaultRingtoneTitle, hasPhoneNumber, lastTimeContacted });
						}
					}
				}
			} else {
				Uri uri = Uri.withAppendedPath(Contacts.CONTENT_GROUP_URI, this.title);
				Cursor groupCursor = getContext().getContentResolver().query(uri, null, null, null, Contacts._ID);

				while (groupCursor.moveToNext()) {
					long contactId = groupCursor.getLong(groupCursor.getColumnIndexOrThrow(Contacts._ID));
					String displayName = groupCursor.getString(groupCursor.getColumnIndexOrThrow(Contacts.DISPLAY_NAME_PRIMARY));
					String lookupKey = groupCursor.getString(groupCursor.getColumnIndexOrThrow(Contacts.LOOKUP_KEY));
					String photoThumbnailUri = groupCursor.getString(groupCursor.getColumnIndexOrThrow(Contacts.PHOTO_THUMBNAIL_URI));
					String customRingtone = groupCursor.getString(groupCursor.getColumnIndexOrThrow(Contacts.CUSTOM_RINGTONE));	
					int hasPhoneNumber = groupCursor.getInt(groupCursor.getColumnIndexOrThrow(Contacts.HAS_PHONE_NUMBER));
					int lastTimeContacted = groupCursor.getInt(groupCursor.getColumnIndexOrThrow(Contacts.LAST_TIME_CONTACTED));
					
					// Ringtone
					if (customRingtone != null) {
						String customRingtoneTitle = map.get(customRingtone);
						mc.addRow(new Object[] { contactId, displayName, lookupKey, photoThumbnailUri, customRingtoneTitle, hasPhoneNumber, lastTimeContacted });
					} else {
						mc.addRow(new Object[] { contactId, displayName, lookupKey, photoThumbnailUri, defaultRingtoneTitle, hasPhoneNumber, lastTimeContacted });
					}
				}
			}
		}
		*/
		if (this.groupId == -1 && this.title.equals("My Contacts")) {
			
		//	Cursor c = getContext().getContentResolver().query(Contacts.CONTENT_URI, new String[] { Contacts._ID, Contacts.DISPLAY_NAME_PRIMARY, Contacts.LOOKUP_KEY, Contacts.PHOTO_THUMBNAIL_URI }, selection, selectionArgs, Contacts._ID);
			Cursor c = getContext().getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, Contacts._ID);
			Cursor c2 = getContext().getContentResolver().query(
					RawContacts.CONTENT_URI,
					null,
					RawContacts.DELETED + " = 0 AND " + RawContacts.ACCOUNT_NAME + " = ? AND " + RawContacts.ACCOUNT_TYPE + " = ?", 
					new String[] { this.accountName, this.accountType },
					RawContacts.CONTACT_ID);

			
			CursorJoinerWithIntKey joiner = new CursorJoinerWithIntKey(c, new String[]{ ContactsContract.Contacts._ID }, c2, new String[] { RawContacts.CONTACT_ID });
			for (CursorJoinerWithIntKey.Result joinerResult : joiner) {
				if (joinerResult == Result.BOTH) {
					long contactId = c.getLong(c.getColumnIndexOrThrow(Contacts._ID));
					String displayName = c.getString(c.getColumnIndexOrThrow(Contacts.DISPLAY_NAME_PRIMARY));
					String lookupKey = c.getString(c.getColumnIndexOrThrow(Contacts.LOOKUP_KEY));
					String photoThumbnailUri = c.getString(c.getColumnIndexOrThrow(Contacts.PHOTO_THUMBNAIL_URI));
					String customRingtone = c.getString(c.getColumnIndexOrThrow(Contacts.CUSTOM_RINGTONE));	
					int hasPhoneNumber = c.getInt(c.getColumnIndexOrThrow(Contacts.HAS_PHONE_NUMBER));
					int lastTimeContacted = c.getInt(c.getColumnIndexOrThrow(Contacts.LAST_TIME_CONTACTED));
					
					// Ringtone
					if (customRingtone != null) {
						String customRingtoneTitle = map.get(customRingtone);
						mc.addRow(new Object[] { contactId, displayName, lookupKey, photoThumbnailUri, customRingtoneTitle, hasPhoneNumber, lastTimeContacted });
					} else {
						mc.addRow(new Object[] { contactId, displayName, lookupKey, photoThumbnailUri, defaultRingtoneTitle, hasPhoneNumber, lastTimeContacted });
					}
				}
			}
			
			
		} else {
			
			Uri uri = Uri.withAppendedPath(Contacts.CONTENT_GROUP_URI, this.title);
			
			Cursor c = getContext().getContentResolver().query(uri, null, null, null, Contacts._ID);

			if (query != null) {
				Cursor c2 = getContext().getContentResolver().query(
						Data.CONTENT_URI,
						null,
						Data.MIMETYPE + " = ? AND " + 
						StructuredName.PHONETIC_NAME + " LIKE '%' || ? || '%' ESCAPE '$'",
						new String[] { StructuredName.CONTENT_ITEM_TYPE, query },
						Data.CONTACT_ID);
				
				CursorJoinerWithIntKey joiner = new CursorJoinerWithIntKey(c, new String[]{ Contacts._ID }, c2, new String[] { Data.CONTACT_ID });
				for (CursorJoinerWithIntKey.Result joinerResult : joiner) {
					if (joinerResult == Result.BOTH) {
						long contactId = c.getLong(c.getColumnIndexOrThrow(Contacts._ID));
						String displayName = c.getString(c.getColumnIndexOrThrow(Contacts.DISPLAY_NAME_PRIMARY));
						String lookupKey = c.getString(c.getColumnIndexOrThrow(Contacts.LOOKUP_KEY));
						String photoThumbnailUri = c.getString(c.getColumnIndexOrThrow(Contacts.PHOTO_THUMBNAIL_URI));
						String customRingtone = c.getString(c.getColumnIndexOrThrow(Contacts.CUSTOM_RINGTONE));
						int hasPhoneNumber = c.getInt(c.getColumnIndexOrThrow(Contacts.HAS_PHONE_NUMBER));
						int lastTimeContacted = c.getInt(c.getColumnIndexOrThrow(Contacts.LAST_TIME_CONTACTED));

						// Ringtone
						if (customRingtone != null) {
							String customRingtoneTitle = map.get(customRingtone);
							mc.addRow(new Object[] { contactId, displayName, lookupKey, photoThumbnailUri, customRingtoneTitle, hasPhoneNumber, lastTimeContacted });
						} else {
							mc.addRow(new Object[] { contactId, displayName, lookupKey, photoThumbnailUri, defaultRingtoneTitle, hasPhoneNumber, lastTimeContacted });
						}
					}
				}
				
				Cursor c3 = getContext().getContentResolver().query(uri, null, Contacts.DISPLAY_NAME_PRIMARY + " LIKE '%' || ? || '%' ESCAPE '$'", new String[] { query }, Contacts._ID);
				
				CursorJoinerWithIntKey joiner2 = new CursorJoinerWithIntKey(mc, new String[]{ Contacts._ID }, c3, new String[] { Contacts._ID });
				for (CursorJoinerWithIntKey.Result joinerResult : joiner2) {
					if (joinerResult == Result.RIGHT) {
						long contactId = c3.getLong(c.getColumnIndexOrThrow(Contacts._ID));
						String displayName = c3.getString(c.getColumnIndexOrThrow(Contacts.DISPLAY_NAME_PRIMARY));
						String lookupKey = c3.getString(c.getColumnIndexOrThrow(Contacts.LOOKUP_KEY));
						String photoThumbnailUri = c3.getString(c.getColumnIndexOrThrow(Contacts.PHOTO_THUMBNAIL_URI));
						String customRingtone = c3.getString(c3.getColumnIndexOrThrow(Contacts.CUSTOM_RINGTONE));
						int hasPhoneNumber = c3.getInt(c3.getColumnIndexOrThrow(Contacts.HAS_PHONE_NUMBER));
						int lastTimeContacted = c3.getInt(c3.getColumnIndexOrThrow(Contacts.LAST_TIME_CONTACTED));
						
						// Ringtone
						if (customRingtone != null) {
							String customRingtoneTitle = map.get(customRingtone);
							mc.addRow(new Object[] { contactId, displayName, lookupKey, photoThumbnailUri, customRingtoneTitle, hasPhoneNumber, lastTimeContacted });
						} else {
							mc.addRow(new Object[] { contactId, displayName, lookupKey, photoThumbnailUri, defaultRingtoneTitle, hasPhoneNumber, lastTimeContacted });
						}
					}
				}
				
			} else {
				Cursor c2 = getContext().getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, Contacts._ID);
				
				String[] columnNames = c2.getColumnNames();
				for (int i = 0; i < columnNames.length; i++) {
					Log.i(LOG_TAG, columnNames[i]);
				}
				
				while (c.moveToNext()) {
					long contactId = c.getLong(c.getColumnIndexOrThrow(Contacts._ID));
					String displayName = c.getString(c.getColumnIndexOrThrow(Contacts.DISPLAY_NAME_PRIMARY));
					String lookupKey = c.getString(c.getColumnIndexOrThrow(Contacts.LOOKUP_KEY));
					String photoThumbnailUri = c.getString(c.getColumnIndexOrThrow(Contacts.PHOTO_THUMBNAIL_URI));
					String customRingtone = c.getString(c.getColumnIndexOrThrow(Contacts.CUSTOM_RINGTONE));	
					int hasPhoneNumber = c.getInt(c.getColumnIndexOrThrow(Contacts.HAS_PHONE_NUMBER));
					int lastTimeContacted = c.getInt(c.getColumnIndexOrThrow(Contacts.LAST_TIME_CONTACTED));
					
					// Ringtone
					if (customRingtone != null) {
						String customRingtoneTitle = map.get(customRingtone);
						mc.addRow(new Object[] { contactId, displayName, lookupKey, photoThumbnailUri, customRingtoneTitle, hasPhoneNumber, lastTimeContacted });
					} else {
						mc.addRow(new Object[] { contactId, displayName, lookupKey, photoThumbnailUri, defaultRingtoneTitle, hasPhoneNumber, lastTimeContacted });
					}
				}
			}
		}

		/*
		
		Cursor c;
		Cursor c2;
		
		if (query != null) {
			c= getContext().getContentResolver().query(uri, null, Contacts.DISPLAY_NAME_PRIMARY + " LIKE '%' || ? || '%' ESCAPE '$'", new String[] { query }, Contacts._ID);
			c2 = getContext().getContentResolver().query(Contacts.CONTENT_URI, null, StructuredName.PHONETIC_NAME + " LIKE '%' || ? || '%' ESCAPE '$'", new String[] { query }, Contacts._ID);
		
			Log.i(LOG_TAG, "none");
			Log.i(LOG_TAG, "COUNT(" + c.getCount() + ", " + c2.getCount() + ")");
		} else {
			c= getContext().getContentResolver().query(uri, null, null, null, Contacts._ID);
		//while (c.moveToNext()) {
		//	long contactId = c.getLong(c.getColumnIndexOrThrow(Contacts._ID));
		//	String displayName = c.getString(c.getColumnIndexOrThrow(Contacts.DISPLAY_NAME_PRIMARY));
		//	String lookupKey = c.getString(c.getColumnIndexOrThrow(Contacts.LOOKUP_KEY));
		//	String photoThumbnailUri = c.getString(c.getColumnIndexOrThrow(Contacts.PHOTO_THUMBNAIL_URI));
			
		//	mc.addRow(new Object[] { contactId, displayName, lookupKey, photoThumbnailUri });
		//}
		
			c2 = getContext().getContentResolver().query(Contacts.CONTENT_URI, null, null, null, Contacts._ID);
			
			Log.i(LOG_TAG, "test");
			Log.i(LOG_TAG, "COUNT(" + c.getCount() + ", " + c2.getCount() + ")");
		}*/
		
		
		/*
		//switch (groupType) {
		//	case Knowledge.GROUP_TYPE_SYSTEM:
		//	{
				// グループ指定
				
				// すべてのアカウントが統合された連絡先を取得
				Cursor c = getContext().getContentResolver().query(
						Contacts.CONTENT_URI,
						new String[]{ Contacts._ID, Contacts.DISPLAY_NAME_PRIMARY, Contacts.LOOKUP_KEY, Contacts.PHOTO_THUMBNAIL_URI },
						selection,
						selectionArgs,
						Contacts._ID);
				// グループに対応した連絡先を取得
				Cursor c2 = getContext().getContentResolver().query(
						Data.CONTENT_URI, 
						new String[]{ GroupMembership.CONTACT_ID, GroupMembership.DISPLAY_NAME_PRIMARY }, 
						Data.MIMETYPE + " = ? AND " + GroupMembership.GROUP_SOURCE_ID + " = ? AND " + RawContacts.ACCOUNT_NAME + " = ? AND " + RawContacts.ACCOUNT_TYPE + " = ?",
						new String[]{ GroupMembership.CONTENT_ITEM_TYPE, groupSourceId, accountName, accountType },
						GroupMembership.CONTACT_ID);
				// 
				CursorJoinerWithIntKey cj = new CursorJoinerWithIntKey(
						c, 
						new String[]{ Contacts._ID },
						c2,
						new String[]{ GroupMembership.CONTACT_ID });
				
				for (CursorJoinerWithIntKey.Result result : cj) {
					switch (result) {
						case LEFT:	
							// handle case where a row in CursorA is unique
							break;
							
						case RIGHT:
							// handle case where a row in CursorB is unique
							break;
	
						case BOTH:
							// handle case where a row with th same key is in both cursors
						{
							Log.i(LOG_TAG, c2.getString(c2.getColumnIndexOrThrow(RawContacts.ACCOUNT_NAME)));
							long contactId = c.getLong(c.getColumnIndexOrThrow(Contacts._ID));
							String displayName = c.getString(c.getColumnIndexOrThrow(Contacts.DISPLAY_NAME_PRIMARY));
							String lookupKey = c.getString(c.getColumnIndexOrThrow(Contacts.LOOKUP_KEY));
							String photoThumbnailUri = c.getString(c.getColumnIndexOrThrow(Contacts.PHOTO_THUMBNAIL_URI));
							
							mc.addRow(new Object[] { contactId, displayName, lookupKey, photoThumbnailUri });
						}
							break;
							
						default:
							break;
					}
				}
	//		}
		//		break;*/
//
	//		case Knowledge.GROUP_TYPE_APP:
		//	{
				/*
				if (groupId == Knowledge.MY_CONTACTS) {
					// すべての連絡先
					
					// すべてのアカウントが統合された連絡先を取得
					Cursor c = getContext().getContentResolver().query(
							Contacts.CONTENT_URI,
							new String[] { Contacts._ID, Contacts.DISPLAY_NAME_PRIMARY, Contacts.LOOKUP_KEY, Contacts.PHOTO_THUMBNAIL_URI },
							selection,
							selectionArgs,
							Contacts._ID);
					// アカウント名、アカウント種別に対応した連絡先を取得
					Cursor c2 = getContext().getContentResolver().query(
							RawContacts.CONTENT_URI, 
							new String[] { RawContacts.CONTACT_ID }, 
							RawContacts.ACCOUNT_NAME + " = ? AND " + RawContacts.ACCOUNT_TYPE + " = ?", 
							new String[] { this.accountName, this.accountType }, 
							RawContacts.CONTACT_ID);
					// 
					CursorJoinerWithIntKey cj = new CursorJoinerWithIntKey(
							c,  new String[] { Contacts._ID },
							c2, new String[] { RawContacts.CONTACT_ID });
					for (CursorJoinerWithIntKey.Result result : cj) {
						switch (result) {
							case LEFT:	
								// handle case where a row in CursorA is unique
								break;
								
							case RIGHT:
								// handle case where a row in CursorB is unique
								break;

							case BOTH:
								// handle case where a row with th same key is in both cursors
							{
								long contactId = c.getLong(c.getColumnIndexOrThrow(Contacts._ID));
								String displayName = c.getString(c.getColumnIndexOrThrow(Contacts.DISPLAY_NAME_PRIMARY));
								String lookupKey = c.getString(c.getColumnIndexOrThrow(Contacts.LOOKUP_KEY));
								String photoThumbnailUri = c.getString(c.getColumnIndexOrThrow(Contacts.PHOTO_THUMBNAIL_URI));
								
								mc.addRow(new Object[] { contactId, displayName, lookupKey, photoThumbnailUri });
							}
								break;
								
							default:
								break;
						}
					}
						
				} else if (groupId == Knowledge.NONE_GROUP_CONTACTS) {
					// グループ設定なし
					
					
				}
				*/
	//		}
		//		break;
//
	//		default:
		//		break;
		//}
		
		return mc5;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onStartLoading() {
		super.onStartLoading();
	
		forceLoad();
	}
}