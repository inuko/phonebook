package net.kokohadoko.instil.phonebook;

/**
 * 
 * 
 * @author inuko
 * @since 0.0.1
 */
public class Email {

	/** メールアドレス */
	private String address;
	/** 種類 */
	private int type;

	/**
	 * デフォルトコンストラクタ
	 * 
	 * @since 0.0.1
	 * @param address メールアドレス
	 * @param type 種類
	 */
	public Email(String address, int type) {
		this.address = address;
		this.type = type;
	}

	/**
	 * メールアドレスを取得する
	 * 
	 * @since 0.0.1
	 * @return メールアドレス
	 * @see {@link #setAddress(String)}
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * メールアドレスを設定する
	 * 
	 * @since 0.0.1
	 * @param address メールアドレス
	 * @see {@link #getAddress()}
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * 種類を取得する
	 * 
	 * @since 0.0.1
	 * @return 種類
	 * @see {@link #setType(int)}
	 */
	public int getType() {
		return type;
	}

	/**
	 * 種類を設定する
	 * 
	 * @since 0.0.1
	 * @param type 種類
	 * @see {@link #getType()}
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * オブジェクトが検索条件に一致するかどうかを返す
	 * 
	 * @since 0.0.1
	 * @param query 抽出条件
	 * @return オブジェクトが検索条件に一致する場合に true, そうでない場合に false を返す
	 */
	public boolean isMatch(String query) {
		if (query != null) {
			if (address != null && address.contains(query)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}