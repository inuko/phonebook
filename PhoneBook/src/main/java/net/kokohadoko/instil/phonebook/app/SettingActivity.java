package net.kokohadoko.instil.phonebook.app;

import net.kokohadoko.instil.phonebook.R;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;

/**
 * アプリ全体の設定を行うためのアクティビティ
 * 
 * @author inuko
 * @since 0.0.1
 */
public class SettingActivity extends ActionBarActivity {

	/** ログ出力用文字列 */
	private static final String LOG_TAG = SettingActivity.class.getSimpleName();

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);

		if (savedInstanceState == null) {
			FragmentManager fm = this.getSupportFragmentManager();
			FragmentTransaction ft = fm.beginTransaction();
			SettingFragment fragment = SettingFragment.newInstance();
			ft.replace(R.id.content_frame, fragment);
			ft.commit();
		}
	}
}
