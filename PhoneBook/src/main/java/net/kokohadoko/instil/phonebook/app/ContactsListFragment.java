package net.kokohadoko.instil.phonebook.app;

import net.kokohadoko.instil.phonebook.CustomRingtone;
import net.kokohadoko.instil.phonebook.R;
import net.kokohadoko.instil.phonebook.content.ContactsLoader;
import net.kokohadoko.instil.phonebook.view.CustomRingtoneListDialog;
import net.kokohadoko.instil.phonebook.widget.ContactsCursorAdapter;
import android.app.ActionBar;
import android.content.ContentValues;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Groups;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ListView;

/**
 * 連絡先の一覧を表示するListFragment
 * 
 * @author inuko
 * @since 0.0.1
 */
public class ContactsListFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor>, OnQueryTextListener, CustomRingtoneListDialog.Callbacks {

	/** 発信確認一括設定（メニューアイテムID）を表す */
	public static final int MENU_ITEM_OUTGOING = 1;
	/** 着信確認一括設定（メニューアイテムID）を表す */
	public static final int MENU_ITEM_INCOMING = 2;
	/** 自動応答一括設定（メニューアイテムID）を表す */
	public static final int MENU_ITEM_REPLY = 3;
	/** 着信音一括設定（メニューアイテムID）を表す */
	public static final int MENU_ITEM_RINGTONE = 4;

	/** ログ出力用文字列 */
	private static final String LOG_TAG = ContactsListFragment.class.getSimpleName();
	/** 抽出条件を表す文字列 */
	private static final String QUERY = "query";

	/** 連絡先一覧を保持するアダプター */
	private ContactsCursorAdapter mAdapter;

	/**
	 * デフォルトコンストラクタ
	 * 
	 * @since 0.0.1
	 */
	public ContactsListFragment() {
	}

	/**
	 * 初期化されたContactsListFragmentインスタンスを制せ宇する
	 * 
	 * @since 0.0.1
	 * @param groupId グループID
	 * @param accountNmae アカウント名
	 * @param accountType アカウント種別
	 * @param title グループ名
	 * @return 初期化されたContactsListFragmentインスタンス
	 */
	public static ContactsListFragment newInstance(long groupId, String accountName, String accountType, String title) {
		ContactsListFragment fragment = new ContactsListFragment();
		Bundle args = new Bundle(4);
		args.putLong(Groups._ID, groupId);
		args.putString(Groups.ACCOUNT_NAME, accountName);
		args.putString(Groups.ACCOUNT_TYPE, accountType);
		args.putString(Groups.TITLE, title);
		fragment.setArguments(args);
		return fragment;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setHasOptionsMenu(true);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.listfragment_contacts, null, false);
		
		return view;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mAdapter = new ContactsCursorAdapter(
				this.getActivity().getApplicationContext(),
				null);
		getListView().setAdapter(mAdapter);
		setListAdapter(mAdapter);

		registerForContextMenu(getListView());
		
		Bundle args = getArguments();
		getLoaderManager().restartLoader(0, args, this);
	}

	/* (非 Javadoc)
	 * @see android.support.v4.app.Fragment#onContextItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onContextItemSelected(MenuItem item) {

		switch (item.getGroupId()) {
			case MainActivity.MENU_GROUP_CONTRACT:
			{
				AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
				
				Cursor cursor = (Cursor) mAdapter.getItem(info.position);
				for (int i = 0; i < cursor.getColumnCount(); i++) {
					Log.i(LOG_TAG, "" + cursor.getColumnName(i) + ":" + cursor.getString(i));
				}
			}
				break;
	
			default:
				break;
		}

		return super.onContextItemSelected(item);
	}

	/* (非 Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateContextMenu(android.view.ContextMenu, android.view.View, android.view.ContextMenu.ContextMenuInfo)
	 */
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);

		AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
		Cursor cursor = (Cursor) mAdapter.getItem(info.position);

		// TODO:着信拒否リストにあるかどうか
		// TODO:ほかの設定条件により以下の内容を変更する
		
		menu.setHeaderTitle("");
		menu.add(MainActivity.MENU_GROUP_CONTRACT, 0, 0, "発信確認 ON/OFF");
		menu.add(MainActivity.MENU_GROUP_CONTRACT, 1, 0, "着信制限 ON/OFF");
		menu.add(MainActivity.MENU_GROUP_CONTRACT, 2, 0, "自動応答 ON/OFF");
		menu.add(MainActivity.MENU_GROUP_CONTRACT, 3, 0, "削除");
		menu.add(MainActivity.MENU_GROUP_CONTRACT, 4, 0, "編集");
		menu.add(MainActivity.MENU_GROUP_CONTRACT, 5, 0, "着信音設定");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//		super.onCreateOptionsMenu(menu, inflater);
		// Fragmentのメニューをクリアする
		menu.clear();
		
		final MenuItem item = menu.add("search");
		item.setIcon(android.R.drawable.ic_menu_search);
		item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

		// 
		SearchView mSearchView = new SearchView(this.getActivity());
		mSearchView.setOnQueryTextListener(this);
		mSearchView.setQueryHint("検索文字を入力してください");
		mSearchView.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				final ActionBar actionBar = getActivity().getActionBar();
				
			}
		});
		// 
		item.setActionView(mSearchView);
		
		// 
		final MenuItem add = menu.add("add");
		add.setIcon(android.R.drawable.ic_menu_add);
		add.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		
		menu.add(MainActivity.MENU_GROUP_MAIN, MainActivity.MENU_ITEM_SETTINGS, Menu.NONE, "設定");
		menu.addSubMenu(MainActivity.MENU_GROUP_CONTRACT, MENU_ITEM_OUTGOING, Menu.NONE, "発信確認へ追加");
		menu.addSubMenu(MainActivity.MENU_GROUP_CONTRACT, MENU_ITEM_INCOMING, Menu.NONE, "着信制限へ追加");
		menu.addSubMenu(MainActivity.MENU_GROUP_CONTRACT, MENU_ITEM_REPLY, Menu.NONE, "自動応答へ追加");
		menu.addSubMenu(MainActivity.MENU_GROUP_CONTRACT, MENU_ITEM_RINGTONE, Menu.NONE, "着信音一括設定");
	}
	
	/* (非 Javadoc)
	 * @see android.support.v4.app.ListFragment#onListItemClick(android.widget.ListView, android.view.View, int, long)
	 */
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
	
		Log.i(LOG_TAG, "onListItemClick");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		switch (item.getGroupId()) {
			case MainActivity.MENU_GROUP_CONTRACT:
			{
				switch (item.getItemId()) {
					case MENU_ITEM_OUTGOING:
					{
						
					}
						break;
						
					case MENU_ITEM_INCOMING:
					{
						
					}
						break;
						
					case MENU_ITEM_REPLY:
					{
						
					}
						break;
						
					case MENU_ITEM_RINGTONE:
					{
						RingtoneManager rm = new RingtoneManager(getActivity().getApplicationContext());
						Cursor cursor = rm.getCursor();
						
						CustomRingtoneListDialog dialog = CustomRingtoneListDialog.newInstance(cursor);
						dialog.setTargetFragment(this, 0);
						FragmentManager fm = this.getActivity().getSupportFragmentManager();
						dialog.show(fm, "dialog");
					}
						break;
	
					default:
						break;
				}
			}
				break;

			default:
				break;
		}
		
		return super.onOptionsItemSelected(item);
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean onQueryTextChange(String newText) {
		
		Bundle args = getArguments();
		
		if (TextUtils.isEmpty(newText)) {
			args.remove(QUERY);
			getLoaderManager().restartLoader(0, args, this);
		} else {
			args.putString(QUERY, newText.toString());
			getLoaderManager().restartLoader(0, args, this);
		}

		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean onQueryTextSubmit(String query) {
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		
		long groupId = args.getLong(Groups._ID);
		String accountName = args.getString(Groups.ACCOUNT_NAME);
		String accountType = args.getString(Groups.ACCOUNT_TYPE);
		String title = args.getString(Groups.TITLE);
		String query = args.getString(QUERY);
		
		ContactsLoader loader = new ContactsLoader(
					getActivity().getApplicationContext(),
					groupId,
					accountName,
					accountType,
					title,
					query 
				);
		return loader;
	}

	/**
	 * {@inheritDoc}
	 */
	public void onLoadFinished(Loader<Cursor> loader, Cursor c) {
		mAdapter.swapCursor(c);
	}

	/**
	 * {@inheritDoc}
	 */
	public void onLoaderReset(Loader<Cursor> loader) {
		mAdapter.swapCursor(null);
	}

	/**
	 * {@inheritDoc}
	 */
	public void onClickCustomRingtone(String title, Uri uri, String lookupKey) {
		Bundle args = getArguments();
		String accountName = args.getString(Groups.ACCOUNT_NAME);
		String accountType = args.getString(Groups.ACCOUNT_TYPE);
		long groupId = args.getLong(Groups._ID);
		String groupTitle = args.getString(Groups.TITLE);
		
		CustomRingtone customRingtone = new CustomRingtone();
		customRingtone.setAccountName(accountName);
		customRingtone.setAccountType(accountType);
		customRingtone.setGroupId(groupId);
		customRingtone.setGroupTitle(groupTitle);
		
		customRingtone.setTitle(title);
		customRingtone.setUri(uri);

		if (lookupKey != null) {
			/**
			 * 個人に対して設定
			 */
			
		} else {
			/**
			 * グループに対して設定
			 */
		
			int count = getActivity().getContentResolver().update(
					CustomRingtone.CONTENT_URI,
					customRingtone.toContentValues(),
					CustomRingtone.ACCOUNT_NAME + " = ? AND " + 
					CustomRingtone.ACCOUNT_TYPE + " = ? AND " +
					CustomRingtone.GROUP_ID_COLUMN + " = ? AND " +
					CustomRingtone.GROUP_TITLE_COLUMN + " = ?",
					new String[] { accountName, accountType, String.valueOf(groupId), groupTitle });
			if (count == 0) {
				Uri insertUri = getActivity().getContentResolver().insert(CustomRingtone.CONTENT_URI, customRingtone.toContentValues());
				if (insertUri == null) {
					
				}
			}
		
			// TODO:	
			if (groupId == -1 && groupTitle.equals("My Contacts")) {
				
			} else {
				
				ContentValues values = new ContentValues(1);
				Uri groupUri = Uri.withAppendedPath(Contacts.CONTENT_GROUP_URI, groupTitle);
				Cursor c = getActivity().getContentResolver().query(groupUri, null, null, null, Contacts._ID);
				while (c.moveToNext()) {
					long contactId = c.getLong(c.getColumnIndexOrThrow(Contacts._ID));
					values.put(Contacts.CUSTOM_RINGTONE, uri.toString());
					int count2 = getActivity().getContentResolver().update(
									Contacts.CONTENT_URI, 
									values, 
									Contacts._ID + " = ?",
									new String[] { String.valueOf(contactId) });
					if (count2 == 0) {
						
					}
				}
			}
		}
	}
}
