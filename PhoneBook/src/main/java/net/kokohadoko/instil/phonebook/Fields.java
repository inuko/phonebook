package net.kokohadoko.instil.phonebook;

/**
 * 連絡先の項目を構成するクラス
 * 
 * @author inuko
 * @since 0.0.1
 */
public abstract class Fields {

	/**  */
	public static final String RAW_CONTACTS_ID = "_id";
	/** ルックアップキーを表す */
	public static final String LOOKUP = "lookup";
	/** 表示名を表す */
	public static final String DISPLAY_NAME = "display_name";
	/** ふりがなを表す */
	public static final String PHONETIC_NAME = "phonetic_name";
	/** 着信音を表す */
	public static final String CUSTOM_RINGTONE = "custom_ringtone";
	/** メモを表す */
	public static final String NOTE = "note";
	/** 会社を表す */
	public static final String COMPANY = "company";
	/** 役職を表す */
	public static final String TITLE = "title";
	/** メールアドレスを表す */
	public static final String EMAIL = "email";
	/** イベントを表す */
	public static final String EVENT = "event";
	/** ニックネームを表す */
	public static final String NICKNAME = "nickname";
	/** 関係を表す */
	public static final String RELATION = "relation";
	/** サムネイル画像URIを表す */
	public static final String PHOTO_THUMBNAIL_URI = "photo_thumbnail_uri";
	/** 電話番号があるかどうかを表す */
	public static final String HAS_PHONE_NUMBER = "has_phone_number";
	/** 最終連絡日時を表す */
	public static final String LAST_TIME_CONTACTED = "last_time_contacted";
	/** 電話番号を表す */
	public static final String NUMBER = "number";

	/**
	 * デフォルトコンストラクタ
	 * 
	 * @since 0.0.1
	 */
	public Fields() {
	}
}