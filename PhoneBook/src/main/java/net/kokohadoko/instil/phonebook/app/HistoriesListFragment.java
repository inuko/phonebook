package net.kokohadoko.instil.phonebook.app;

import net.kokohadoko.instil.phonebook.R;
import net.kokohadoko.instil.phonebook.content.HistoriesLoader;
import net.kokohadoko.instil.phonebook.widget.HistoriesCursorAdapter;
import android.app.ActionBar;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract.Groups;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

/**
 * 電話履歴の一覧を表示するListFragment
 * 
 * @author inuko
 * @since 0.0.1
 */
public class HistoriesListFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor>, OnQueryTextListener {

	/** 発信確認一括設定（メニューアイテムID）を表す */
	public static final int MENU_ITEM_OUTGOING = 1;
	/** 着信確認一括設定（メニューアイテムID）を表す */
	public static final int MENU_ITEM_INCOMING = 2;
	/** 自動応答一括設定（メニューアイテムID）を表す */
	public static final int MENU_ITEM_REPLY = 3;

	/** ログ出力用文字列 */
	private static final String LOG_TAG = HistoriesListFragment.class.getSimpleName();
	/** 抽出条件を表す文字列 */
	private static final String QUERY = "query";

	/** 電話履歴の一覧を保持するアダプター */
	private HistoriesCursorAdapter mAdapter;

	/**
	 * デフォルトコンストラクタ
	 * 
	 * @since 0.0.1
	 */
	public HistoriesListFragment() {
	}

	/**
	 * 初期化されたHistoriesListFragmentインスタンスを生成する
	 * 
	 * @since 0.0.1
	 * @param groupId グループID
	 * @param accountName アカウント名
	 * @param accountType アカウント種別
	 * @param title グループ名
	 * @return 初期化されたHistoriesListFragmentインスタンス
	 */
	public static HistoriesListFragment newInstance(long groupId, String accountName, String accountType, String title) {
		HistoriesListFragment fragment = new HistoriesListFragment();
		Bundle args = new Bundle(4);
		args.putLong(Groups._ID, groupId);
		args.putString(Groups.ACCOUNT_NAME, accountName);
		args.putString(Groups.ACCOUNT_TYPE, accountType);
		args.putString(Groups.TITLE, title);
		fragment.setArguments(args);
		return fragment;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setHasOptionsMenu(true);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.listfragment_histories, null, false);
		return view;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mAdapter = new HistoriesCursorAdapter(
				this.getActivity().getApplicationContext(),
				null);
		getListView().setAdapter(mAdapter);
		setListAdapter(mAdapter);

		Bundle args = getArguments();
		getLoaderManager().restartLoader(0, args, this);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//		super.onCreateOptionsMenu(menu, inflater);
		// Fragmentのメニューをクリアする
		menu.clear();
		
		final MenuItem item = menu.add("search");
		item.setIcon(android.R.drawable.ic_menu_search);
		item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

		// 
		SearchView mSearchView = new SearchView(this.getActivity());
		mSearchView.setOnQueryTextListener(this);
		mSearchView.setQueryHint("検索文字を入力してください");
		mSearchView.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				final ActionBar actionBar = getActivity().getActionBar();
				
			}
		});
		// 
		item.setActionView(mSearchView);
		
		// 
		final MenuItem add = menu.add("add");
		add.setIcon(android.R.drawable.ic_menu_add);
		add.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		
		menu.add(MainActivity.MENU_GROUP_MAIN, MainActivity.MENU_ITEM_SETTINGS, Menu.NONE, "設定");
		menu.addSubMenu(MainActivity.MENU_GROUP_HISTORIES, MENU_ITEM_OUTGOING, Menu.NONE, "発信確認へ追加");
		menu.addSubMenu(MainActivity.MENU_GROUP_HISTORIES, MENU_ITEM_INCOMING, Menu.NONE, "着信制限へ追加");
		menu.addSubMenu(MainActivity.MENU_GROUP_HISTORIES, MENU_ITEM_REPLY, Menu.NONE, "自動応答へ追加");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		switch (item.getGroupId()) {
			case MainActivity.MENU_GROUP_HISTORIES:
			{
				switch (item.getItemId()) {
					case MENU_ITEM_OUTGOING:
					{
						
					}
						break;
	
					case MENU_ITEM_INCOMING:
					{
						
					}
						break;
						
					case MENU_ITEM_REPLY:
					{
						
					}
						break;
	
					default:
					break;
				}
			}
				break;
				
			default:
				break;
		}
		
		return super.onOptionsItemSelected(item);
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean onQueryTextChange(String newText) {
		
		Bundle args = getArguments();
		
		if (TextUtils.isEmpty(newText)) {
			args.remove(QUERY);
			getLoaderManager().restartLoader(0, args, this);
		} else {
			args.putString(QUERY, newText.toString());
			getLoaderManager().restartLoader(0, args, this);
		}

		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean onQueryTextSubmit(String query) {
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {

		long groupId = args.getLong(Groups._ID);
		String accountName = args.getString(Groups.ACCOUNT_NAME);
		String accountType = args.getString(Groups.ACCOUNT_TYPE);
		String title = args.getString(Groups.TITLE);
		String query = args.getString(QUERY);

		HistoriesLoader loader = new HistoriesLoader(
					getActivity().getApplicationContext(),
					groupId,
					accountName,
					accountType,
					title,
					query
				);
		return loader;
	}

	/**
	 * {@inheritDoc}
	 */
	public void onLoadFinished(Loader<Cursor> loader, Cursor c) {
		mAdapter.swapCursor(c);
	}

	/**
	 * {@inheritDoc}
	 */
	public void onLoaderReset(Loader<Cursor> loader) {
		mAdapter.swapCursor(null);
	}
}