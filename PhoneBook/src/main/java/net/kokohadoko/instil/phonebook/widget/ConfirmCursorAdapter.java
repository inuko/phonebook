package net.kokohadoko.instil.phonebook.widget;

import net.kokohadoko.instil.phonebook.R;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.QuickContactBadge;
import android.widget.TextView;

/**
 * 電話発信の確認に確認する連絡先の一覧を保持するアダプター
 * 
 * @author inuko
 * @since 0.0.1
 */
public class ConfirmCursorAdapter extends CursorAdapter {

	/** ログ出力用文字列 */
	private static final String LOG_TAG = ConfirmCursorAdapter.class.getSimpleName();

	/** LayoutInflater */
	private LayoutInflater inflater;

	/**
	 * コンストラクタ
	 * 
	 * @since 0.0.1
	 * @param context コンテキスト
	 * @param c カーソル
	 */
	public ConfirmCursorAdapter(Context context, Cursor c) {
		super(context, c, false);

		inflater = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void bindView(View view, Context context, Cursor c) {

		long contactId = c.getInt(c.getColumnIndexOrThrow(Contacts._ID));
		String lookupKey = c.getString(c.getColumnIndexOrThrow(Contacts.LOOKUP_KEY));
		String displayNamePrimary = c.getString(c.getColumnIndexOrThrow(Contacts.DISPLAY_NAME_PRIMARY));
		String photoThumbnailUri = c.getString(c.getColumnIndexOrThrow(Contacts.PHOTO_THUMBNAIL_URI));

		QuickContactBadge quickContact = (QuickContactBadge) view.findViewById(R.id.quickContactBadge);
		if (photoThumbnailUri != null) {
			quickContact.setImageURI(Uri.parse(photoThumbnailUri));
			quickContact.setMode(ContactsContract.QuickContact.MODE_MEDIUM);
			quickContact.assignContactUri(Contacts.getLookupUri(contactId, lookupKey));
		} else {
			quickContact.setImageToDefault();
			quickContact.setMode(ContactsContract.QuickContact.MODE_MEDIUM);
			quickContact.assignContactUri(Contacts.getLookupUri(contactId, lookupKey));
		}

		TextView textViewDisplayNamePrimary = (TextView) view.findViewById(R.id.textView_displayNamePrimary);
		textViewDisplayNamePrimary.setText(displayNamePrimary);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public View newView(Context context, Cursor c, ViewGroup parent) {
		View view = inflater.inflate(R.layout.row_contact, null, false);
		return view;
	}
}