package net.kokohadoko.instil.phonebook;

import java.util.ArrayList;
import java.util.List;

import net.kokohadoko.instil.android.content.SharedPreferencesHelper;
import android.content.Context;

/**
 * 連絡先を構成するクラス
 * 
 * @author inuko
 * @since 0.0.1
 */
public class Person {

	/** 連絡先ID */
	private long rawContactsId;
	/** ルックアップキー */
	private String lookup;
	/** サムネイル画像URI */
	private String photoThumbnailUri;
	/** 電話番号をもっているかどうか */
	private int hasPhoneNumber;
	/** 最終連絡日時 */
	private String lastTimeContacted;
	/** 着信音 */
	private String customRingtone;
	/** 表示名 */
	private String displayName;
	/** ふりがな */
	private String phoneticName;
	/** メモ */
	private String note;
	/** 会社 */
	private String company;
	/** 役職 */
	private String title;
	/** メールアドレス */
	private List<Email> email;
	/** イベント */
	private List<Event> event;
	/** ニックネーム */
	private Nickname nickname;
	/** 関係 */
	private Relation relation;
	/** 電話番号 */
	private List<Phone> phone;
	// TODO:住所を追加

	/**
	 * コンストラクタ
	 * 
	 * @since 0.0.1
	 * @param rawContactsId 連絡先ID
	 * @param lookup ルックアップキー
	 * @param photoThumbnailUri サムネイル画像URI
	 * @param hasPhoneNumber 電話番号をもっているかどうか
	 * @param lastTimeContacted 最終連絡日時
	 * @param customRingtone 着信音
	 */
	public Person(long rawContactsId, String lookup, String photoThumbnailUri, int hasPhoneNumber, String lastTimeContacted, String customRingtone) {
		this.rawContactsId = rawContactsId;
		this.lookup = lookup;
		this.photoThumbnailUri = photoThumbnailUri;
		this.hasPhoneNumber = hasPhoneNumber;
		this.lastTimeContacted = lastTimeContacted;
		this.customRingtone = customRingtone;

		this.email = new ArrayList<Email>();
		this.event = new ArrayList<Event>();
		this.phone = new ArrayList<Phone>();
	}

	/**
	 * 連絡先IDを取得する
	 * 
	 * @since 0.0.1
	 * @return 連絡先ID
	 * @see {@link #setRawContactsId(long)}
	 */
	public long getRawContactsId() {
		return rawContactsId;
	}

	/**
	 * 連絡先IDを設定する
	 * 
	 * @since 0.0.1
	 * @param rawContactsId 連絡先ID
	 * @see {@link #getRawContactsId()}
	 */
	public void setRawContactsId(long rawContactsId) {
		this.rawContactsId = rawContactsId;
	}

	/**
	 * ルックアップキーを取得する
	 * 
	 * @since 0.0.1
	 * @return lookup ルックアップキー
	 * @see {@link #setLookup(String)}
	 */
	public String getLookup() {
		return lookup;
	}

	/**
	 * ルックアップキーを設定する
	 * 
	 * @since 0.0.1
	 * @param lookup ルックアップキー
	 * @see {@link #getLookup()}
	 */
	public void setLookup(String lookup) {
		this.lookup = lookup;
	}

	/**
	 * 着信音を取得する
	 * 
	 * @since 0.0.1
	 * @return 着信音
	 * @see {@link #setCustomRingtone(String)}
	 */
	public String getCustomRingtone() {
		return customRingtone;
	}

	/**
	 * 着信音を設定する
	 * 
	 * @since 0.0.1
	 * @param customRingtone 着信音
	 * @see {@link #getCustomRingtone()}
	 */
	public void setCustomRingtone(String customRingtone) {
		this.customRingtone = customRingtone;
	}

	/**
	 * 表示名を取得する
	 * 
	 * @since 0.0.1
	 * @return 表示名
	 * @see {@link #setDisplayName(String)}
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * 表示名を設定する
	 * 
	 * @since 0.0.1
	 * @param displayName 表示名
	 * @see {@link #getDisplayName()}
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * ふりがなを取得する
	 * 
	 * @since 0.0.1
	 * @return ふりがな
	 * @see {@link #setPhoneticName(String)}
	 */
	public String getPhoneticName() {
		return phoneticName;
	}

	/**
	 * ふりがなを設定する
	 * 
	 * @since 0.0.1
	 * @param phoneticName ふりがな
	 * @see {@link #getPhoneticName()}
	 */
	public void setPhoneticName(String phoneticName) {
		this.phoneticName = phoneticName;
	}

	/**
	 * ふりがなを設定する
	 * 
	 * @since 0.0.1
	 * @param familyName 
	 * @param middleName 
	 * @param givenName 
	 */
	public void setPhoneticName(String familyName, String middleName, String givenName) {
		
	}

	/**
	 * メモを取得する
	 * 
	 * @since 0.0.1
	 * @return メモ
	 * @see {@link #setNote(String)}
	 */
	public String getNote() {
		return note;
	}

	/**
	 * メモを設定する
	 * 
	 * @since 0.0.1
	 * @param note メモ
	 * @see {@link #getNote()}
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 会社名を取得する
	 * 
	 * @since 0.0.1
	 * @return 会社名
	 * @see {@link #setCompany(String)}
	 */
	public String getCompany() {
		return company;
	}

	/**
	 * 会社名を設定する
	 * 
	 * @since 0.0.1
	 * @param company 会社名
	 * @see {@link #getCompany()}
	 */
	public void setCompany(String company) {
		this.company = company;
	}

	/**
	 * 役職を取得する
	 * 
	 * @since 0.0.1
	 * @return 役職
	 * @see {@link #setTitle(String)}
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 役職を設定する
	 * 
	 * @since 0.0.1
	 * @param title 役職
	 * @see {@link #getTitle()}
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * メールアドレスを取得する
	 * 
	 * @since 0.0.1
	 * @return メールアドレス
	 * @see {@link #setEmail(List)}
	 */
	public List<Email> getEmail() {
		return email;
	}

	/**
	 * メールアドレスを設定する
	 * 
	 * @since 0.0.1
	 * @param email メールアドレス
	 * @see {@link #getEmail()}
	 */
	public void setEmail(List<Email> email) {
		this.email = email;
	}

	/**
	 * メールアドレスを追加する
	 * 
	 * @since 0.0.1
	 * @param email メールアドレス
	 */
	public void addEmail(Email email) {
		this.email.add(email);
	}

	/**
	 * イベントを取得する
	 * 
	 * @since 0.0.1
	 * @return event イベント
	 * @see {@link #setEvent(List)}
	 */
	public List<Event> getEvent() {
		return event;
	}

	/**
	 * イベントを設定する
	 * 
	 * @since 0.0.1
	 * @param event イベント
	 * @see {@link #getEvent()}
	 */
	public void setEvent(List<Event> event) {
		this.event = event;
	}

	/**
	 * イベントを追加する
	 * 
	 * @since 0.0.1
	 * @param event イベント
	 */
	public void addEvent(Event event) {
		this.event.add(event);
	}

	/**
	 * ニックネームを取得する
	 * 
	 * @since 0.0.1
	 * @return nickname ニックネーム
	 * @see {@link #setNickname(Nickname)}
	 */
	public Nickname getNickname() {
		return nickname;
	}

	/**
	 * ニックネームを設定する
	 * 
	 * @since 0.0.1
	 * @param nickname ニックネーム
	 * @see {@link #getNickname()}
	 */
	public void setNickname(Nickname nickname) {
		this.nickname = nickname;
	}

	/**
	 * 関係を取得する
	 * 
	 * @since 0.0.1
	 * @return 関係
	 * @see {@link #setRelation(Relation)}
	 */
	public Relation getRelation() {
		return relation;
	}

	/**
	 * 関係を設定する
	 * 
	 * @since 0.0.1
	 * @param relation 関係
	 * @see {@link #getRelation()}
	 */
	public void setRelation(Relation relation) {
		this.relation = relation;
	}

	/**
	 * 電話番号を取得する
	 * 
	 * @since 0.0.1
	 * @return 電話番号
	 * @see {@link #setPhone(List)}
	 */
	public List<Phone> getPhone() {
		return phone;
	}

	/**
	 * 電話番号を設定する
	 * 
	 * @since 0.0.1
	 * @param phone 電話番号
	 * @see {@link #getPhone()}
	 */
	public void setPhone(List<Phone> phone) {
		this.phone = phone;
	}

	/**
	 * 電話番号を追加する
	 * 
	 * @since 0.0.1
	 * @param phone 電話番号
	 */
	public void addPhone(Phone phone) {
		this.phone.add(phone);
	}

	/**
	 * サムネイル画像URIを取得する
	 * 
	 * @since 0.0.1
	 * @return サムネイル画像URI
	 * @see {@link #setPhotoThumbnailUri(String)}
	 */
	public String getPhotoThumbnailUri() {
		return photoThumbnailUri;
	}

	/**
	 * サムネイル画像URIを設定する
	 * 
	 * @since 0.0.1
	 * @param photoThumbnailUri サムネイル画像URI
	 * @see {@link #getPhotoThumbnailUri()}
	 */
	public void setPhotoThumbnailUri(String photoThumbnailUri) {
		this.photoThumbnailUri = photoThumbnailUri;
	}

	/**
	 * オブジェクトが検索条件に一致するかどうかを返す
	 * 
	 * @since 0.0.1
	 * @param query 抽出条件
	 * @return オブジェクトが検索条件に一致する場合に true, そうでない場合に false を返す
	 */
	public static boolean isMatch(Context context, Person person, String query) {

		if (person != null) {
			if (query != null) {
				SharedPreferencesHelper pref = new SharedPreferencesHelper(context);
				// 表示名
				if (pref.getBoolean(SearchableFields.DISPLAY_NAME, false)) {
					String displayName = person.getDisplayName();
					if (displayName != null && displayName.contains(query)) {
						return true;
					}
				}
				// ふりがな
				if (pref.getBoolean(SearchableFields.PHONETIC_NAME, false)) {
					String phoneticName = person.getPhoneticName();
					if (phoneticName != null && phoneticName.contains(query)) {
						return true;
					}
				}
				// 着信音
				if (pref.getBoolean(SearchableFields.CUSTOM_RINGTONE, false)) {
					String customRingtone = person.getCustomRingtone();
					if (customRingtone != null && customRingtone.contains(query)) {
						return true;
					}
				}
				// メモ
				if (pref.getBoolean(SearchableFields.NOTE, false)) {
					String note = person.getNote();
					if (note != null && note.contains(query)) {
						return true;
					}
				}
				// 会社
				if (pref.getBoolean(SearchableFields.COMPANY, false)) {
					String company = person.getCompany();
					if (company != null && company.contains(query)) {
						return true;
					}
				}
				// 役職
				if (pref.getBoolean(SearchableFields.TITLE, false)) {
					String title = person.getTitle();
					if (title != null && title.contains(query)) {
						return true;
					}
				}
				// メールアドレス
				if (pref.getBoolean(SearchableFields.EMAIL, false)) {
					List<Email> emails = person.getEmail();
					int size = emails.size();
					for (int i = 0; i < size; i++) {
						Email email = emails.get(i);
						if (email.isMatch(query)) {
							return true;
						}
					}
				}
				// イベント
				if (pref.getBoolean(SearchableFields.EVENT, false)) {
					List<Event> events = person.getEvent();
					int size = events.size();
					for (int i = 0; i < size; i++) {
						Event event = events.get(i);
						if (event.isMatch(query)) {
							return true;
						}
					}
				}
				// ニックネーム
				if (pref.getBoolean(SearchableFields.NICKNAME, false)) {
					Nickname nickname = person.getNickname();
					if (nickname != null && nickname.isMatch(query)) {
						return true;
					}
				}
				// 関係
				if (pref.getBoolean(SearchableFields.RELATION, false)) {
					Relation relation = person.getRelation();
					if (relation != null && relation.isMatch(query)) {
						return true;
					}
				}
				// 電話番号
				if (pref.getBoolean(SearchableFields.NUMBER, false)) {
					List<Phone> phones = person.getPhone();
					int size = phones.size();
					for (int i = 0; i < size; i++) {
						Phone phone = phones.get(i);
						String number = phone.getNumber();
						if (number != null && number.contains(query)) {
							return true;
						}
					}
				}

				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

	/**
	 * オブジェクト配列に変換して返す
	 * 
	 * @since 0.0.1
	 * @return オブジェクト配列に変換したもの
	 */
	public Object[] toObjects() {
		
		// TODO:
		/** メールアドレス */
		//private List<Email> email;
		/** イベント */
		//private List<Event> event;
		/** ニックネーム */
		//private Nickname nickname;
		/** 関係 */
		//private Relation relation;

		return new Object[] {
				this.rawContactsId,
				this.lookup,
				this.photoThumbnailUri,
				this.hasPhoneNumber,
				this.lastTimeContacted,
				this.customRingtone,
				this.displayName,
				this.phoneticName,
				this.note,
				this.company,
				this.title
		};
	}
}