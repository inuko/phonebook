package net.kokohadoko.instil.phonebook.content;

import net.kokohadoko.instil.phonebook.ConfirmContacts;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorJoiner;
import android.database.CursorJoiner.Result;
import android.database.MatrixCursor;
import android.provider.ContactsContract.Contacts;
import android.support.v4.content.AsyncTaskLoader;

/**
 * 電話発信を確認する連絡先の一覧を取得するローダー
 * 
 * @author inuko
 * @since 0.0.1
 */
public class ConfirmLoader extends AsyncTaskLoader<Cursor> {

	/**
	 * コンストラクタ
	 * 
	 * @since 0.0.1
	 * @param context コンテキスト
	 */
	public ConfirmLoader(Context context) {
		super(context);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Cursor loadInBackground() {

		MatrixCursor mc = new MatrixCursor(
				new String [] {
						Contacts._ID,
						Contacts.DISPLAY_NAME_PRIMARY,
						Contacts.LOOKUP_KEY, 
						Contacts.PHOTO_THUMBNAIL_URI
				}
		);

		Cursor cursor1 = getContext().getContentResolver().query(ConfirmContacts.CONTENT_URI, null, null, null, ConfirmContacts.LOOKUP_KEY);		
		Cursor cursor2 = getContext().getContentResolver().query(Contacts.CONTENT_URI, null, null, null, Contacts.LOOKUP_KEY);

		CursorJoiner joiner = new CursorJoiner(cursor1, new String[]{ ConfirmContacts.LOOKUP_KEY }, cursor2, new String[] { Contacts.LOOKUP_KEY });
		for (CursorJoiner.Result joinerResult : joiner) {
			if (joinerResult == Result.BOTH) {

				long contactId = cursor2.getLong(cursor2.getColumnIndexOrThrow(Contacts._ID));
				String displayName = cursor2.getString(cursor2.getColumnIndexOrThrow(Contacts.DISPLAY_NAME_PRIMARY));
				String lookupKey = cursor2.getString(cursor2.getColumnIndexOrThrow(Contacts.LOOKUP_KEY));
				String photoThumbnailUri = cursor2.getString(cursor2.getColumnIndexOrThrow(Contacts.PHOTO_THUMBNAIL_URI));

				mc.addRow(new Object[] { contactId, displayName, lookupKey, photoThumbnailUri });
			}
		}

		return mc;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onStartLoading() {
		super.onStartLoading();
	
		forceLoad();
	}
}
