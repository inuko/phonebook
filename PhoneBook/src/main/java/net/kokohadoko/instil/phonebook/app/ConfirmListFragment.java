package net.kokohadoko.instil.phonebook.app;

import net.kokohadoko.instil.phonebook.R;
import net.kokohadoko.instil.phonebook.content.ConfirmLoader;
import net.kokohadoko.instil.phonebook.widget.ConfirmCursorAdapter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

/**
 * 電話発信の場合に確認する連絡先一覧を表示するListFragment
 * 
 * @author inuko
 * @since 0.0.1
 */
public class ConfirmListFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor>, CompoundButton.OnCheckedChangeListener {

	/** ログ出力用文字列 */
	private static final String LOG_TAG = ConfirmListFragment.class.getSimpleName();

	/** SharedPreferences */
	private SharedPreferences pref;

	/** 電話発信の場合に確認する連絡先一覧を保持するアダプター */
	private ConfirmCursorAdapter mAdapter;

	/**
	 * デフォルトコンストラクタ
	 * 
	 * @since 0.0.1
	 */
	public ConfirmListFragment() {
	}

	/**
	 * 初期化されたConfirmListFragmentインスタンスを生成する
	 * 
	 * @since 0.0.1
	 * @return 初期化されたConfirmListFragmentインスタンス
	 */
	public static ConfirmListFragment newInstance() {
		ConfirmListFragment fragment = new ConfirmListFragment();
		return fragment;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.listfragment_confirm, null, false);

		

		return view;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		mAdapter = new ConfirmCursorAdapter(
				this.getActivity().getApplicationContext(),
				null);
		getListView().setAdapter(mAdapter);
		setListAdapter(mAdapter);

		getLoaderManager().restartLoader(0, null, this);
	}

	/**
	 * {@inheritDoc}
	 */
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		/*
		switch (buttonView.getId()) {
			case R.id.switch_confirm:
			{
				// 
				pref = this.getActivity().getSharedPreferences("pref", Activity.MODE_PRIVATE);
				Editor e = pref.edit();
				e.putBoolean("confirm", isChecked);
				// データの保存
				e.commit();
			}
				break;
				
			default:
				break;
		}*/
	}

	/**
	 * {@inheritDoc}
	 */
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		ConfirmLoader loader = new ConfirmLoader(getActivity().getApplicationContext());
		return loader;
	}

	/**
	 * {@inheritDoc}
	 */
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		mAdapter.swapCursor(cursor);
	}

	/**
	 * {@inheritDoc}
	 */
	public void onLoaderReset(Loader<Cursor> loader) {
		mAdapter.swapCursor(null);
	}
}