package net.kokohadoko.instil.phonebook;

/**
 * 連絡先アカウントを構成するクラス
 * 
 * @author inuko
 * @since 0.0.1
 */
public class ContactAccount {

	/** アカウント名 */
	private String name;
	/** パッケージ名 */
	private String packageName;
	/** アカウント種別 */
	private String type;
	/** ラベルID */
	private int labelId;
	/** アイコンID */
	private int iconId;
	/** アイコンID */
	private int smallIconId;

	/**
	 * デフォルトコンストラクタ
	 * 
	 * @since 0.0.1
	 */
	public ContactAccount() {
	}

	/**
	 * コンストラクタ
	 * 
	 * @since 0.0.1
	 * @param name アカウント名
	 * @param packageName パッケージ名
	 * @param type アカウント種別
	 * @param labelId ラベルID
	 * @param iconId アイコンID
	 * @param smallIconId アイコンID
	 */
	public ContactAccount(String name, String packageName, String type, int labelId, int iconId, int smallIconId) {
		this.name = name;
		this.packageName = packageName;
		this.type = type;
		this.labelId = labelId;
		this.iconId = iconId;
		this.smallIconId = smallIconId;
	}

	/**
	 * アカウント名を取得する
	 * 
	 * @since 0.0.1
	 * @return アカウント名
	 * @see {@link #setName(String)}
	 */
	public String getName() {
		return name;
	}

	/**
	 * アカウント名を設定する
	 * 
	 * @since 0.0.1
	 * @param name アカウント名
	 * @see {@link #getName()}
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * パッケージ名を取得する
	 * 
	 * @since 0.0.1
	 * @return パッケージ名
	 * @see {@link #setPackageName(String)}
	 */
	public String getPackageName() {
		return packageName;
	}

	/**
	 * パッケージ名を設定する
	 * 
	 * @since 0.0.1
	 * @param packageName パッケージ名
	 * @see {@link #getPackageName()}
	 */
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	/**
	 * アカウント種別を取得する
	 * 
	 * @since 0.0.1
	 * @return アカウント種別
	 * @see {@link #setType(String)}
	 */
	public String getType() {
		return type;
	}

	/**
	 * アカウント種別を設定する
	 * 
	 * @since 0.0.1
	 * @param type アカウント種別
	 * @see {@link #getType()}
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * ラベルIDを取得する
	 * 
	 * @since 0.0.1
	 * @return ラベルID
	 * @see {@link #setLabelId(int)}
	 */
	public int getLabelId() {
		return labelId;
	}

	/**
	 * ラベルIDを設定する
	 * 
	 * @since 0.0.1
	 * @param labelId ラベルID
	 * @see {@link #getLabelId()}
	 */
	public void setLabelId(int labelId) {
		this.labelId = labelId;
	}

	/**
	 * アイコンIDを取得する
	 * 
	 * @since 0.0.1
	 * @return アイコンID
	 * @see {@link #setIconId(int)}
	 */
	public int getIconId() {
		return iconId;
	}

	/**
	 * アイコンIDを設定する
	 * 
	 * @since 0.0.1
	 * @param iconId アイコンID
	 * @see {@link #getIconId()}
	 */
	public void setIconId(int iconId) {
		this.iconId = iconId;
	}

	/**
	 * アイコンIDを取得する
	 * 
	 * @since 0.0.1
	 * @return アイコンID
	 * @see {@link #setSmallIconId(int)}
	 */
	public int getSmallIconId() {
		return smallIconId;
	}

	/**
	 * アイコンIDを設定する
	 * 
	 * @since 0.0.1
	 * @param smallIconId アイコンID
	 * @see {@link #getSmallIconId()}
	 */
	public void setSmallIconId(int smallIconId) {
		this.smallIconId = smallIconId;
	}
}
