package net.kokohadoko.instil.phonebook.app;

import net.kokohadoko.instil.phonebook.R;
import net.kokohadoko.instil.phonebook.content.ReplyLoader;
import net.kokohadoko.instil.phonebook.widget.ReplyCursorAdapter;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

/**
 * 不在着信の場合に自動的に返信するメッセージの一覧を表示するListFragment
 * 
 * @author inuko
 * @since 0.0.1
 */
public class ReplyListFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor>, CompoundButton.OnCheckedChangeListener {

	/** ログ出力用文字列 */
	private static final String LOG_TAG = ReplyListFragment.class.getSimpleName();

	/** 不在着信の場合に自動返信を行う内容の一覧を保持するアダプター */
	private ReplyCursorAdapter mAdapter;

	/**
	 * デフォルトコンストラクタ
	 * 
	 * @since 0.0.1
	 */
	public ReplyListFragment() {
	}

	/**
	 * 初期化されたReplyListFragmentインスタンスを生成する
	 * 
	 * @since 0.0.1
	 * @return 初期化されたReplyListFragmentインスタンス
	 */
	public static ReplyListFragment newInstance() {
		ReplyListFragment fragment = new ReplyListFragment();
		return fragment;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.listfragment_reply, null, false);


		return view;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		mAdapter = new ReplyCursorAdapter(
				this.getActivity().getApplicationContext(),
				null);
		getListView().setAdapter(mAdapter);
		setListAdapter(mAdapter);

		getLoaderManager().restartLoader(0, null, this);
	}

	/**
	 * {@inheritDoc}
	 */
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		
	}

	/**
	 * {@inheritDoc}
	 */
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		ReplyLoader loader = new ReplyLoader(this.getActivity().getApplicationContext());
		return loader;
	}

	/**
	 * {@inheritDoc}
	 */
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		mAdapter.swapCursor(cursor);
	}

	/**
	 * {@inheritDoc}
	 */
	public void onLoaderReset(Loader<Cursor> loader) {
		mAdapter.swapCursor(null);
	}
}