package net.kokohadoko.instil.phonebook;

/**
 * 連絡先の項目のニックネームを構成するクラス
 * 
 * @author inuko
 * @since 0.0.1
 */
public class Nickname {

	/** ログ出力用文字列 */
	private static final String LOG_TAG = Nickname.class.getSimpleName();

	/** 名前 */
	private String name;
	/** ラベル */
	private String label;
	/** 種類 */
	private int type;

	/**
	 * コンストラクタ
	 * 
	 * @since 0.0.1
	 * @param name 名前
	 * @param label ラベル
	 * @param type 種類
	 */
	public Nickname(String name, String label, int type) {
		this.name = name;
		this.label = label;
		this.type = type;
	}

	/**
	 * 名前を取得する
	 * 
	 * @since 0.0.1
	 * @return name 名前
	 * @see {@link #setName(String)}
	 */
	public String getName() {
		return name;
	}

	/**
	 * 名前を設定する
	 * 
	 * @since 0.0.1
	 * @param name 名前
	 * @see {@link #getName()}
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * ラベルを取得する
	 * 
	 * @since 0.0.1
	 * @return ラベル
	 * @see {@link #setLabel(String)}
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * ラベルを設定する
	 * 
	 * @since 0.0.1
	 * @param label ラベル
	 * @see {@link #getLabel()}
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * 種類を取得する
	 * 
	 * @since 0.0.1
	 * @return type 種類
	 * @see {@link #setType(int)}
	 */
	public int getType() {
		return type;
	}

	/**
	 * 種類を設定する
	 * 
	 * @since 0.0.1
	 * @param type 種類
	 * @see {@link #getType()}
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * オブジェクトが検索条件に一致するかどうかを返す
	 * 
	 * @since 0.0.1
	 * @param query 抽出条件
	 * @return オブジェクトが検索条件に一致する場合に true, そうでない場合に false を返す
	 */
	public boolean isMatch(String query) {
		if (query != null) {
			if (label != null && label.contains(query)) {
				return true;
			} else if (name != null && name.contains(query)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}