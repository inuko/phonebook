package net.kokohadoko.instil.phonebook;

import java.net.URI;

import net.kokohadoko.instil.phonebook.content.PhoneBookContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.ContactsContract.Groups;

/**
 * グループ単位で設定する着信音を構成するクラス
 * 
 * @author inuko
 * @since 0.0.1
 */
public class CustomRingtone implements BaseColumns {

	/** ログ出力用 */
	public static final String LOG_TAG = CustomRingtone.class.getSimpleName();

	/** テーブル固有のCONTENT_URI．ContentResolverからこれを使用してアクセスを行う． */
	public static final Uri CONTENT_URI = Uri.parse("content://" + PhoneBookContentProvider.AUTHORITY + "/custom_ringtone");
	/** MIME_TYPE（複数） */
	public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.instil.custom_ringtone";
	/** MIME_TYPE（単数） */
	public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.instil.custom_ringtone";
	/** テーブル名 */
	public static final String TABLE_NAME = "custom_ringtone";

	/** アカウント名（カラム名）を表す */
	public static final String ACCOUNT_NAME = Groups.ACCOUNT_NAME;
	/** アカウント種別（カラム名）を表す */
	public static final String ACCOUNT_TYPE = Groups.ACCOUNT_TYPE;
	/** グループID（カラム名）を表す */
	public static final String GROUP_ID_COLUMN = "groupId";
	/** グループ名（カラム名）を表す */
	public static final String GROUP_TITLE_COLUMN = "groupTitle";
	/** タイトル（カラム名）を表す */
	public static final String TITLE_COLUMN = "title";
	/** 着信音URI（カラム名）を表す */
	public static final String URI_COLUMN = "uri";

	/** アカウント名 */
	private String accountName;
	/** アカウント種別 */
	private String accountType;
	/** グループID */
	private long groupId;
	/** グループ名 */
	private String groupTitle;
	/** 着信音タイトル */
	private String title;
	/** 着信音URI */
	private Uri uri;

	/**
	 * デフォルトコンストラクタ
	 * 
	 * @since 0.0.1
	 */
	public CustomRingtone() {
	}

	/**
	 * コンストラクタ
	 * 
	 * @since 0.0.1
	 * @param cursor カーソル
	 */
	public CustomRingtone(Cursor cursor) {
		this.accountName = cursor.getString(cursor.getColumnIndexOrThrow(ACCOUNT_NAME));
		this.accountType = cursor.getString(cursor.getColumnIndexOrThrow(ACCOUNT_TYPE));
		this.groupId = cursor.getLong(cursor.getColumnIndexOrThrow(GROUP_ID_COLUMN));
		this.groupTitle = cursor.getString(cursor.getColumnIndexOrThrow(GROUP_TITLE_COLUMN));
		this.title = cursor.getString(cursor.getColumnIndexOrThrow(TITLE_COLUMN));
		this.uri = Uri.parse(cursor.getString(cursor.getColumnIndexOrThrow(URI_COLUMN)));
	}

	/**
	 * アカウント名を取得する
	 * 
	 * @since 0.0.1
	 * @return アカウント名
	 * @see {@link #getAccountName()}
	 */
	public String getAccountName() {
		return accountName;
	}

	/**
	 * アカウント名を設定する
	 * 
	 * @since 0.0.1
	 * @param accountName アカウント名
	 * @see {@link #getAccountName()}
	 */
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	/**
	 * アカウント種別を取得する
	 * 
	 * @since 0.0.1
	 * @return アカウント種別
	 * @see {@link #setAccountType(String)}
	 */
	public String getAccountType() {
		return accountType;
	}

	/**
	 * アカウント種別を設定する
	 * 
	 * @since 0.0.1
	 * @param accountType アカウント種別
	 * @see {@link #getAccountType()}
	 */
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	/**
	 * グループIDを取得する
	 * 
	 * @see {@link #setGroupId(long)}
	 * @since 0.0.1
	 * @return グループID
	 */
	public long getGroupId() {
		return groupId;
	}

	/**
	 * グループIDを設定する
	 * 
	 * @see {@link #getGroupId()}
	 * @since 0.0.1
	 * @param groupId グループID
	 */
	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}

	/**
	 * グループ名を取得する
	 * 
	 * @since 0.0.1
	 * @return グループ名
	 * @see {@link #setGroupTitle(String)}
	 */
	public String getGroupTitle() {
		return groupTitle;
	}

	/**
	 * グループ名を設定する
	 * 
	 * @since 0.0.1
	 * @param groupTitle グループ名
	 * @see {@link #getGroupTitle()}
	 */
	public void setGroupTitle(String groupTitle) {
		this.groupTitle = groupTitle;
	}

	/**
	 * 着信音タイトルを取得する
	 * 
	 * @see {@link #setTitle(String)}
	 * @since 0.0.1
	 * @return 着信音タイトル
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 着信音タイトルを設定する
	 * 
	 * @see {@link #setTitle(String)}
	 * @since 0.0.1
	 * @param title 着信音タイトル
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 着信音URIを取得する
	 * 
	 * @see {@link #setUri(URI)}
	 * @since 0.0.1
	 * @return 着信音URI
	 */
	public Uri getUri() {
		return uri;
	}

	/**
	 * 着信音URIを設定する
	 * 
	 * @see {@link #getUri()}
	 * @since 0.0.1
	 * @param uri 着信音URI
	 */
	public void setUri(Uri uri) {
		this.uri = uri;
	}

	/**
	 * SQLite用のCREATE TABLE文を生成する
	 * 
	 * @since 0.0.1
	 * @return CREATE TABLE文
	 */
	public static String getSQLForCreateTable() {
		StringBuffer buf = new StringBuffer();
		buf.append("CREATE TABLE ").append(TABLE_NAME).append("(");
		buf.append(_ID).append(" INTEGER PRIMARY_KEY, ");
		buf.append(ACCOUNT_NAME).append(" TEXT NOT NULL, ");
		buf.append(ACCOUNT_TYPE).append(" TEXT NOT NULL, ");
		buf.append(GROUP_ID_COLUMN).append(" INTEGER NOT NULL, ");
		buf.append(GROUP_TITLE_COLUMN).append(" TEXT NOT NULL, ");
		buf.append(TITLE_COLUMN).append(" TEXT NOT NULL, ");
		buf.append(URI_COLUMN).append(" TEXT NOT NULL");
		buf.append(")");
		return buf.toString();
	}

	/**
	 * オブジェクトをContentValues形式に変換する
	 * 
	 * @since 0.0.1
	 * @return ContentValues形式に変換されたオブジェクト
	 */
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues(6);
		values.put(ACCOUNT_NAME, this.accountName);
		values.put(ACCOUNT_TYPE, this.accountType);
		values.put(GROUP_ID_COLUMN, this.groupId);
		values.put(GROUP_TITLE_COLUMN, this.groupTitle);
		values.put(TITLE_COLUMN, this.title);
		values.put(URI_COLUMN, this.uri.toString());
		return values;
	}
}
