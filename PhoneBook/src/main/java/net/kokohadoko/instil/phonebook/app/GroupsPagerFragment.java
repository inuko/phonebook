package net.kokohadoko.instil.phonebook.app;

import net.kokohadoko.instil.phonebook.R;
import android.os.Bundle;
import android.provider.ContactsContract.Groups;
import android.provider.ContactsContract.RawContacts;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * グループ単位で表示するFragmentを管理するViewPagerを構成するFragment
 * 
 * @author inuko
 * @since 0.0.1
 */
public class GroupsPagerFragment extends Fragment {

	/** 連絡先一覧画面のページタイトルを表す文字列 */
	public static final String PAGE_TITLE_CONTACT = "連絡先一覧";
	/** 履歴一覧画面のページタイトルを表す文字列 */
	public static final String PAGE_TITLE_HISTORY = "履歴一覧";

	/** ログ出力用文字列 */
	private static final String LOG_TAG = GroupsPagerFragment.class.getSimpleName();
	/** オフスクリーンで保持するFragment枚数 */
	private static final int OFF_SCREEN_PAGE_LIMIT = 2;

	/** ViewPager */
	private ViewPager mViewPager;
	
	/**
	 * デフォルトコンストラクタ
	 * 
	 * @since 0.0.1
	 */
	public GroupsPagerFragment() {
	}

	/**
	 * 初期化されたGroupsPagerFragmentインスタンスを生成する
	 * 
	 * @since 0.0.1
	 * @param groupId グループID
	 * @param accountName アカウント名
	 * @param accountType アカウント種別
	 * @param title グループ名
	 * @return 初期化されたGroupsPagerFragmentインスタンス
	 */
	public static GroupsPagerFragment newInstance(long groupId, String accountName, String accountType, String title) {
		GroupsPagerFragment fragment = new GroupsPagerFragment();
		Bundle args = new Bundle(4);
		args.putLong(Groups._ID, groupId);
		args.putString(Groups.ACCOUNT_NAME, accountName);
		args.putString(Groups.ACCOUNT_TYPE, accountType);
		args.putString(Groups.TITLE, title);
		fragment.setArguments(args);
		return fragment;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_groups_view_pager, null, false);

		mViewPager = (ViewPager) view.findViewById(R.id.pager);
		mViewPager.setOffscreenPageLimit(OFF_SCREEN_PAGE_LIMIT);
		
		return view;
	}

	/**
	 * {@inheritDoc}
	 * @param savedInstanceState
	 */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		Bundle args = getArguments();
		long groupId = args.getLong(Groups._ID);
		String accountName = args.getString(RawContacts.ACCOUNT_NAME);
		String accountType = args.getString(RawContacts.ACCOUNT_TYPE);
		String title = args.getString(Groups.TITLE);
		PagerAdapter adapter = new PagerAdapter(this.getFragmentManager(), groupId, accountName, accountType, title);
		mViewPager.setAdapter(adapter);
	}

	/**
	 * グループ単位で表示する各Fragmentを構成するページャ
	 * 
	 * @author inuko
	 * @since 0.0.1
	 */
	private class PagerAdapter extends FragmentStatePagerAdapter {

		/** グループID */
		private long groupId;
		/** アカウント名 */
		private String accountName;
		/** アカウント種別 */
		private String accountType;
		/** グループ名 */
		private String title;

		/**
		 * コンストラクタ
		 * 
		 * @since 0.0.1
		 * @param fm FragmentManager
		 * @param groupId グループID 
		 * @param accountName アカウント名
		 * @param accountType アカウント種別
		 * @param title グループ名
		 */
		public PagerAdapter(FragmentManager fm, long groupId, String accountName, String accountType, String title) {
			super(fm);

			this.groupId = groupId;
			this.accountName = accountName;
			this.accountType = accountType;
			this.title = title;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Fragment getItem(int position) {
			switch (position) {
				case 0:
				{
					ContactsListFragment fragment = ContactsListFragment.newInstance(groupId, accountName, accountType, title);
					return fragment;
				}

				case 1:
				{
					HistoriesListFragment fragment = HistoriesListFragment.newInstance(groupId, accountName, accountType, title);
					return fragment;
				}

				default:
					return new Fragment();	
			}			
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public int getCount() {
			return 2;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public CharSequence getPageTitle(int position) {

			CharSequence pageTitle;
			
			switch (position) {
				case 0:
				{
					pageTitle = PAGE_TITLE_CONTACT;
				}
					break;
					
				case 1:
				{
					pageTitle = PAGE_TITLE_HISTORY;
				}
					break;
					
				default:
				{
					pageTitle = "";
				}
					break;
			}

			return pageTitle;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public int getItemPosition(Object object) {
			return POSITION_NONE;
		}
	}
}
