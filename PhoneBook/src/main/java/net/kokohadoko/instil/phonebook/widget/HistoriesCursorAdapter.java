package net.kokohadoko.instil.phonebook.widget;

import net.kokohadoko.instil.phonebook.Calls;
import net.kokohadoko.instil.phonebook.Fields;
import net.kokohadoko.instil.phonebook.R;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.QuickContactBadge;
import android.widget.TextView;

/**
 * 電話履歴の一覧を保持するアダプター
 * 
 * @author inuko
 * @since 0.0.1
 */
public class HistoriesCursorAdapter extends CursorAdapter {

	/** ログ出力用文字列 */
	private static final String LOG_TAG = HistoriesCursorAdapter.class.getSimpleName();

	/** LayoutInflater */
	private LayoutInflater inflater;

	/**
	 * コンストラクタ
	 * 
	 * @since 0.0.1
	 * @param context コンテキスト
	 * @param c カーソル
	 */
	public HistoriesCursorAdapter(Context context, Cursor c) {
		super(context, c, false);
		
		inflater = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void bindView(View view, Context context, Cursor c) {
		long contactId = c.getInt(c.getColumnIndexOrThrow(Fields.RAW_CONTACTS_ID));
		String lookupKey = c.getString(c.getColumnIndexOrThrow(Fields.LOOKUP));
		String displayNamePrimary = c.getString(c.getColumnIndexOrThrow(Fields.DISPLAY_NAME));
		String photoThumbnailUri = c.getString(c.getColumnIndexOrThrow(Fields.PHOTO_THUMBNAIL_URI));
		String number = c.getString(c.getColumnIndexOrThrow(Calls.NUMBER));
		long date = c.getLong(c.getColumnIndexOrThrow(Calls.DATE));
		long duration = c.getLong(c.getColumnIndexOrThrow(Calls.DURATION));
		int type = c.getInt(c.getColumnIndexOrThrow(Calls.TYPE));

		QuickContactBadge quickContact = (QuickContactBadge) view.findViewById(R.id.quickContactBadge);
		if (photoThumbnailUri != null) {
			quickContact.setImageURI(Uri.parse(photoThumbnailUri));
			quickContact.setMode(ContactsContract.QuickContact.MODE_MEDIUM);
			quickContact.assignContactUri(Contacts.getLookupUri(contactId, lookupKey));
		} else {
			quickContact.setImageToDefault();
			quickContact.setMode(ContactsContract.QuickContact.MODE_MEDIUM);
			quickContact.assignContactUri(Contacts.getLookupUri(contactId, lookupKey));
		}

		TextView textViewDisplayName = (TextView) view.findViewById(R.id.textView_displayNamePrimary);
		textViewDisplayName.setText(displayNamePrimary);

		TextView textViewNumber = (TextView) view.findViewById(R.id.textView_number);
		textViewNumber.setText(number);
		
		TextView textViewDate = (TextView) view.findViewById(R.id.textView_date);
		textViewDate.setText("" + date);
		
		TextView textViewDuration = (TextView) view.findViewById(R.id.textView_duration);
		textViewDuration.setText("" + duration);
		
		TextView textViewType = (TextView) view.findViewById(R.id.textView_type);
		textViewType.setText("" + type);
		
		ImageView imageViewIncoming = (ImageView) view.findViewById(R.id.imageView_incoming);
		ImageView imageViewMissed = (ImageView) view.findViewById(R.id.imageView_missed);
		ImageView imageViewOutgoing = (ImageView) view.findViewById(R.id.imageView_outgoing);
		
		switch (type) {
			case CallLog.Calls.INCOMING_TYPE:
			{
				imageViewIncoming.setVisibility(View.VISIBLE);
				imageViewMissed.setVisibility(View.INVISIBLE);
				imageViewOutgoing.setVisibility(View.INVISIBLE);
			}
				break;
				
			case CallLog.Calls.MISSED_TYPE:
			{
				imageViewIncoming.setVisibility(View.INVISIBLE);
				imageViewMissed.setVisibility(View.VISIBLE);
				imageViewOutgoing.setVisibility(View.INVISIBLE);
			}
				break;
				
			case CallLog.Calls.OUTGOING_TYPE:
			{
				imageViewIncoming.setVisibility(View.INVISIBLE);
				imageViewMissed.setVisibility(View.INVISIBLE);
				imageViewOutgoing.setVisibility(View.VISIBLE);
			}
				break;
				
			default:
			{
				imageViewIncoming.setVisibility(View.INVISIBLE);
				imageViewMissed.setVisibility(View.INVISIBLE);
				imageViewOutgoing.setVisibility(View.INVISIBLE);
			}
				break;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public View newView(Context context, Cursor c, ViewGroup parent) {
		View view = inflater.inflate(R.layout.row_history, null, false);
		return view;
	}
}