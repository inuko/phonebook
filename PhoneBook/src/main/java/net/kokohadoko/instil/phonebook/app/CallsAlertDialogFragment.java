package net.kokohadoko.instil.phonebook.app;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * 電話発信時に割り込んで確認を行うためのDialogFragment
 * 
 * @author inuko
 * @since 0.0.1
 */
public class CallsAlertDialogFragment extends DialogFragment implements OnClickListener {

	/** タグ用文字列 */
	public static final String TAG = CallsAlertDialogFragment.class.getSimpleName();

	/** ログ出力用文字列 */
	private static final String LOG_TAG = CallsAlertDialogFragment.class.getSimpleName();

	/** タイトルを表す文字列 */
	public static final String TITLE = "title";
	/** メッセージを表す文字列 */
	public static final String MESSAGE = "message";
	/** 電話番号を表す文字列 */
	public static final String NUMBER = "number";

	/**
	 * デフォルトコンストラクタ
	 * 
	 * @since 0.0.1
	 */
	public CallsAlertDialogFragment() {
	}

	/**
	 * 初期化されたCallsAlertDialogFragmentインスタンスを生成する
	 * 
	 * @since 0.0.1
	 * @param title タイトル
	 * @param message メッセージ
	 * @param number 電話番号
	 * @return 初期化されたCallsAlertDialogFragmentインスタンス
	 */
	public static CallsAlertDialogFragment newInstance(String title, String message, String number) {
		CallsAlertDialogFragment fragment = new CallsAlertDialogFragment();
		Bundle args = new Bundle(3);
		args.putString(TITLE, title);
		args.putString(MESSAGE, message);
		args.putString(NUMBER, number);
		fragment.setArguments(args);
		return fragment;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		Bundle args = getArguments();
		String title = args.getString(TITLE);
		String message = args.getString(MESSAGE);
		String number = args.getString(NUMBER);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setPositiveButton("OK", this);
		builder.setNegativeButton("キャンセル", this);
		
		return builder.create();
	}

	/**
	 * {@inheritDoc}
	 */
	public void onClick(DialogInterface dialog, int which) {
		switch (which) {
			case DialogInterface.BUTTON_POSITIVE:
			{
				Bundle args = getArguments();
				String number = args.getString(NUMBER);
				Uri uri = Uri.parse("tel:" + number + "#Confirm");
				Intent intent = new Intent(Intent.ACTION_CALL, uri);
				startActivity(intent);
			}
				break;
				
			case DialogInterface.BUTTON_NEGATIVE:
			{
				
			}
				break;

			default:
				break;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onStop() {
		super.onStop();
		this.getActivity().finish();
	}
}