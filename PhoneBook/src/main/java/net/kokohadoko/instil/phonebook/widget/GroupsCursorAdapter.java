package net.kokohadoko.instil.phonebook.widget;

import net.kokohadoko.instil.phonebook.R;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract.Groups;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * アカウントに属するグループ一覧を保持するアダプター
 * 
 * @author inuko
 * @since 0.0.1
 */
public class GroupsCursorAdapter extends CursorAdapter {

	/** ログ出力用文字列 */
	private static final String LOG_TAG = GroupsCursorAdapter.class.getSimpleName();

	/** LayoutInflater */
	private LayoutInflater inflater;

	/**
	 * コンストラクタ
	 * 
	 * @since 0.0.1
	 * @param context コンテキスト
	 * @param c カーソル
	 */
	public GroupsCursorAdapter(Context context, Cursor c) {
		super(context, c, false);

		inflater = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void bindView(View view, Context context, Cursor c) {
		String title = c.getString(c.getColumnIndexOrThrow(Groups.TITLE));
		TextView textViewGroup = (TextView) view.findViewById(R.id.textView_group);
		textViewGroup.setText(title);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public View newView(Context context, Cursor c, ViewGroup parent) {
		View view = inflater.inflate(R.layout.row_group, null, false);
		return view;
	}
}